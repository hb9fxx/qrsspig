# Try to find libpulseaudio
#
# Once successfully done this will define
#  LIB

include(LibFindMacros)

find_path(LibPulseAudio_INCLUDE_DIR NAMES pulse/pulseaudio.h)

# Search first where lib is installed from source, then system one
find_library(LibPulseAudio_LIBRARY NAMES libpulse pulse PATHS /usr/local/lib /usr/lib NO_DEFAULT_PATH)
find_library(LibPulseAudio_LIBRARY NAMES libpulse pulse)

libfind_process(LibPulseAudio)
