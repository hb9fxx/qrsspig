cmake_minimum_required (VERSION 3.0.2)

cmake_policy(SET CMP0048 NEW)

project (qrsspig VERSION 0.8.0)

set (QRSSPIG_NAME QrssPiG)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

option(WITH_INPUT_AIRSPY "Build with AirSpy input support" OFF)
option(WITH_INPUT_AIRSPYHF "Build with AirSpy HF+ input support" OFF)
option(WITH_INPUT_ALSA "Build with Alsa input support" ON)
option(WITH_INPUT_GROSMOSDR "Build with GR Osmo SDR input support" OFF)
option(WITH_INPUT_HACKRF "Build with HackRF input support" ON)
option(WITH_INPUT_LIMESDR "Build with LimeSDR input support" OFF)
option(WITH_INPUT_PULSEAUDIO "Build with PulseAudio input support" ON)
option(WITH_INPUT_RTLSDR "Build with RtlSdr input support" ON)
option(WITH_INPUT_SDRPLAY "Build with SdrPlay input support" OFF)

option(WITH_UPLOADER_FTP "Build with Ftp uploader support" ON)
option(WITH_UPLOADER_SCP "Build with Scp uploader support" ON)

subdirs(src)
