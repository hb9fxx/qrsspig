# QrssPiG

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)
[![Build Status](https://gitlab.com/hb9fxx/qrsspig/badges/dev/build.svg)](https://gitlab.com/hb9fxx/qrsspig/commits/dev)

QrssPiG is short for QRSS (Raspberry)Pi Grabber.

Haven't found a headless standalone grabber for my Pi. So I try to create my own.

## Functionality
 - Headless QRSS grabber
 - Process I/Q stream from an rtl-sdr or HackRF device
 - Process I/Q stream from a stereo audio input or audio stream from a mono audio input. Currently only 16 bit audio supported
 - Generate pretty horizontal or vertical waterfall graphs with configurable colormaps
 - Upload them via scp or ftp, or just save locally. Or any combination of uploads and local saves

 Some of the colormaps are taken from Scott Harden's Spectrogram: https://github.com/swharden/Spectrogram

## Sample Output
<img src="https://www.dxzone.com/dx32901/qrsspig.jpg" alt="QrssPig Sample Output">

## Configuration
The description of the configuration file is at https://gitlab.com/hb9fxx/qrsspig/wikis/Yaml-config-file

## Installation
You can either install QrssPiG from binaries or build it from source.

### Install binaries
Binaries are available for Debian/Ubuntu as well as for SuSE

#### Install from Debian/Ubuntu repository
There is a repository with binaries for amd64 (PCs), i386 (old PCs), armhf (Raspberry and other arm boards running in 32bit) and arm64 (Raspberry and other arm boards running in 64 bit) for several Raspbian, Debian, Ubuntu and Linux Mint distributions. Supported distributions/architectures are:

| Release                | i386               | amd64              | armhf              | arm64              |
|------------------------|--------------------|--------------------|--------------------|--------------------|
| Raspbian 9 (Stretch)   |                    |                    | :heavy_check_mark: |                    |
| Raspbian 10 (Buster)   |                    |                    | :heavy_check_mark: |                    |
| Raspbian 11 (Bullseye) |                    |                    | :heavy_check_mark: |                    |
| Debian 9 (Stretch)     | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Debian 10 (Buster)     | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Debian 11 (Bullseye)   | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Ubuntu 18.04 (Bionic)  | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| Ubuntu 20.04 (Focal)   |                    | :heavy_check_mark: |                    | :heavy_check_mark: |
| Ubuntu 22.04 (Jammy)   |                    | :heavy_check_mark: |                    | :heavy_check_mark: |

To add the repository, create a file /etc/apt/sources.list.d/hb9fxx.list containing the following two lines:

```
deb https://debian.hb9fxx.ch/debian/ buster/
deb-src https://debian.hb9fxx.ch/debian/ buster/
```

You must replace the 'buster' keyword with your distribution:
- Debian: 'stretch' 'buster' or bullseye
- Ubuntu/Mint: 'bionic', 'focal', 'hirsute', 'impish' or 'jammy'

Linux Mint can use Ubuntu packages, Linux Mint 19 needs 'bionic' and Linux Mint 20 needs 'focal'

Don't forget the trailing '/'. The debian keywords must not be changed, even if you use Ubuntu/Mint.

After having created this file, run:
```
sudo apt-get install apt-transport-https
wget https://debian.hb9fxx.ch/debian/key.asc -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get install qrsspig
```

#### Install from SuSE repository
Thanks to Martin Hauke there are SuSE binaries available. For this you'll need to add the hardware:sdr repository.
e.g. for openSUSE Leap 42.3:
```
$ sudo zypper addrepo -f
https://download.opensuse.org/repositories/hardware:/sdr/openSUSE_Leap_42.3/hardware:sdr.repo
$ sudo zypper install QrssPiG
```

### Build from source
#### Dependencies
To build QrssPiG you need cmake and a c++ compiler (g++ or clang++) with support for c++11.

To build the core of QrssPiG you need the following mandatory dev libs:
 - libboost-filesystem-dev
 - libyaml-cpp-dev
 - libfftw3-dev
 - libgd-dev
 - libliquid-dev: Downsampling of input signal samplerate before processing

Without any additional libs QrssPiG is limited to read data from its standard input and save the output to the local filesystem.

Additional functionalities are available through following libs:
 - Input plugins:
  - libasound2-dev: Alsa audio card
  - libhackrf-dev: HackRF device
  - libpulse-dev: PulseAudio input
  - librtlsdr-dev: RtlSdr device
 - Upload plugins:
  - libcurl4-openssl-dev: FTP upload of grabs to a server
  - libssh-dev: SCP upload of grabs to a server

On Debian/Ubuntu they can all be installed with the following command:
```
$ sudo apt install libboost-filesystem-dev libyaml-cpp-dev libfftw3-dev libgd-dev libliquid-dev libasound2-dev libhackrf-dev libpulse-dev librtlsdr-dev libcurl4-openssl-dev libssh-dev
```

#### Get the sources
Clone the git repository from https://github.com/MartinHerren/QrssPiG.git

or download the latest snapshot from https://github.com/MartinHerren/QrssPiG/archive/master.zip
To have access to the latest developments you can use the dev branch instead of the master.

#### Build:
Inside the QrssPiG directory:
```
$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install
```

## Running QrssPiG
To get a short help about QrssPiG's usage, type
```
$ qrsspig -h
```

To get the list of input/upload modules QrssPiG has been compiled with, type
```
$ qrsspig -m
```

To list found input devices, type
```
$ qrsspig -l
```

You can combine the options -m and -l:
```
$ qrsspig -ml
```
To run QrssPiG, type:
```
$ qrsspig configfile.yaml
```

A simple template for a config file is contained in the git repository: qrsspig.yaml.template, some simple example are given in the next section. For a complete documentation of all options in the config file, refer to the wiki.

Tips and Tricks on running QrssPiG can be found at https://gitlab.com/hb9fxx/qrsspig/wikis/Tips-and-Tricks

If you have an error when accessing the rtl-sdr device:
```
QrssPiG v0.7.0
Configuring input
Created input:rtlsdr[0]
Opening rtlsdr: Generic RTL2832U OEM
usb_open error -3
Please fix the device permissions, e.g. by installing the udev rules file rtl-sdr.rules
Error: Failed to open device
```

You probably need to add your user to the plugdev group:
```
$ sudo usermod -aG plugdev your_username
```

Similarly if you have troubles accessing your audio device, you might need to add your user to the audio group:
```
$ sudo usermod -aG audio your_username
```

## Example configs
The full documentation of the config file format is found at https://gitlab.com/hb9fxx/qrsspig/wikis/Yaml-config-file

### Simple acquisition from a mono soundcard:
This is a typical config for a RaspberryPi with a cheap external mono USB sound dongle (almost all the cheap ones only feature a mono input and a stereo output, sold as 7.1 output). This kind of config is even able to run on the simple first generation RaspberryPi 1 with a single core running at 700MHz and only 256MB of RAM. It can also run on a ReapberryPi Zero(W).
```
input:
  type: alsa
  device: hw:1
  channel: mono
  samplerate: 48000
  basefreq: 10138500
processing:
  fft: 65536
  fftoverlap: 1
output:
  orientation: horizontal
  minutesperframe: 10
  freqmin: 1000
  freqmax: 2000
  dBmin: -50
  dBmax: -20
upload:
  type: local
  dir: /tmp
```

### Simple acquisition from an rtlsdr dongle
This is a typical config for a RaspberryPi with a cheap rtl-sdr dongle. This config is able to run on RaspberryPi 2/3.
```
input:
  type: rtlsdr
  samplerate: 240000
  basefreq: 27999300
  gain: 24
processing:
  fft: 262144
  fftoverlap: 1
output:
  orientation: horizontal
  minutesperframe: 10
  freqmin: 1000
  freqmax: 2000
  dBmin: -50
  dBmax: -20
upload:
  type: local
  dir: /tmp
```

### Simple acquisition from an HackRF
This config involves downsampling from 8MS/s to 8kS/s so it is CPU intensive and needs a PC to run.
```
input:
  type: hackrf
  samplerate: 8000000
  basefreq: 27999300
  amplifier: on
  lnagain: 24
  vgagain: 32
processing:
  samplerate: 8000
  fft: 8192
  fftoverlap: 1
output:
  orientation: horizontal
  minutesperframe: 10
  freqmin: 1000
  freqmax: 2000
  dBmin: -50
  dBmax: -20
upload:
  type: local
  dir: /tmp
```
