#pragma once

#include "QGOutput.h"

#include <complex>
#include <memory>
#include <string>
#include <vector>

#include <gd.h>
#include <yaml-cpp/yaml.h>

class QGLevelMeter : public QGOutput {
public:
	QGLevelMeter(const YAML::Node &config, int index);
	~QGLevelMeter();

	void addLine(const std::complex<float> *fft) override;

protected:
	void _configureNewSource(std::shared_ptr<QGPlugin> const & source) override;

private:
	// Configuration
	unsigned int _fftSize;
};
