#include "QGLevelMeter.h"
#include "Config.h"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <stdexcept>

#include "QGInputDevice.h"
#include "QGProcessor.h"
#include "QGUtils.h"

extern "C" QGOutput* create_object(const YAML::Node &config, int index) {
        return new QGLevelMeter(config, index);
}

QGLevelMeter::QGLevelMeter(const YAML::Node &config, int index)
: QGOutput{"levelmeter", index, config} {
  auto connections = this->connections();
  if (connections.size() != 0) {
    for (auto const& c: connections) {
      std::cout << "Warning: LevelMeter doesn't support uploaders, connection to \"" << c.getName() << "\" will be ignored" << std::endl;
    }
  }

  // Set having connections to set explicitly the connections to empty instead of default to all
  _has_connections = true;
}

QGLevelMeter::~QGLevelMeter() {
}

void QGLevelMeter::addLine(const std::complex<float> *fft) {
	float avg = 0;

	for (unsigned int i = 0; i < _fftSize; i++) {
		float v = 10 * log10(abs(fft[i]) / _fftSize);
		if (std::isnan(v)) continue;

		avg += v;
	}

  std::cout << std::fixed << std::setprecision(2) << std::setw(6) << avg / _fftSize << " dB " << QGUtils::levelBar(avg / _fftSize) << "\r" << std::flush;
}

// Protected
// Override
void QGLevelMeter::_configureNewSource(std::shared_ptr<QGPlugin> const & source) {
    auto processor = std::dynamic_pointer_cast<QGProcessor>(source);

    _fftSize = processor->fftSize();
}
