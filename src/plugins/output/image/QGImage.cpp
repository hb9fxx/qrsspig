#include "QGImage.h"
#include "Config.h"

#include <cmath>
#include <iomanip>
#include <sstream>
#include <stdexcept>

#include "QGFileData.h"
#include "QGInputDevice.h"
#include "QGProcessor.h"
#include "QGUtils.h"

extern "C" QGOutput* create_object(const YAML::Node &config, int index) {
    return new QGImage(config, index);
}

struct _imBufferDeleter {
    void operator()(char* b) const {
        gdFree(b);
    }
};

class QGImageFileData : public QGFileData {
public:
    QGImageFileData(
        std::unique_ptr<char[], _imBufferDeleter> imBuffer,
        int imBufferSize,
        const std::string& fileNameExt,
        double dispFreq,
        std::chrono::milliseconds frameStart
    ) :
    QGFileData{fileNameExt, dispFreq, frameStart},
    _imBufferSize{imBufferSize} {
        _imBuffer = std::move(imBuffer);
    };

    char const * getData() { return _imBuffer.get(); };
    unsigned int getDataSize() { return _imBufferSize; };

private:
    std::unique_ptr<char[], _imBufferDeleter> _imBuffer;
    int _imBufferSize;
};

QGImage::QGImage(const YAML::Node &config, int index) :
QGOutput{"image", index, config},
_started{std::chrono::milliseconds(0)},
_runningSince{std::chrono::milliseconds(0)},
_im{nullptr} {
    _currentLine = 0;
    // QGImage config

    // File name
    _fileNameTmpl = "%I_%fHz";
    if (config["filename"]) _fileNameTmpl = _config["filename"].as<std::string>();

    // File format, default to png
    _format = Format::PNG;
    _fileNameExt = "png";
    if (_config["format"]) {
        std::string f = _config["format"].as<std::string>();

        if (f.compare("png") == 0) _format = Format::PNG;
        else if (f.compare("jpg") == 0) _format = Format::JPG;
        else throw std::runtime_error("QGImage::QGImage: output format unrecognized");
    }

    switch (_format) {
        case Format::PNG:
        _fileNameExt = "png";
        break;
        case Format::JPG:
        _fileNameExt = "jpg";
        break;
    }

    // Configure orientation
    _orientation = Orientation::Horizontal;
    if (_config["orientation"]) {
        std::string o = _config["orientation"].as<std::string>();

        if (o.compare("horizontal") == 0) _orientation = Orientation::Horizontal;
        else if (o.compare("vertical") == 0) _orientation = Orientation::Vertical;
        else throw std::runtime_error("QGImage::configure: output orientation unrecognized");
    }

    if (_config["colormap"]) {
        std::string colorMap = _config["colormap"].as<std::string>();

        if (colorMap == "qrsspig") _colorMap.setMap(ColorMap::Map::QrssPiG);
        else if (colorMap == "grayscale") _colorMap.setMap(ColorMap::Map::Grayscale);
        else if (colorMap == "grayscale_r") _colorMap.setMap(ColorMap::Map::GrayscaleR);
        else if (colorMap == "argos") _colorMap.setMap(ColorMap::Map::Argos);
        else if (colorMap == "blues") _colorMap.setMap(ColorMap::Map::Blues);
        else if (colorMap == "greens") _colorMap.setMap(ColorMap::Map::Greens);
        else if (colorMap == "inferno") _colorMap.setMap(ColorMap::Map::Inferno);
        else if (colorMap == "lopora") _colorMap.setMap(ColorMap::Map::Lopora);
        else if (colorMap == "magma") _colorMap.setMap(ColorMap::Map::Magma);
        else if (colorMap == "plasma") _colorMap.setMap(ColorMap::Map::Plasma);
        else if (colorMap == "turbo") _colorMap.setMap(ColorMap::Map::Turbo);
        else if (colorMap == "viridis") _colorMap.setMap(ColorMap::Map::Viridis);
        else throw std::runtime_error("[QGImage] Error: unsupported colormap value");
    }

    _secondsPerFrame = 10 * 60;
    if (_config["minutesperframe"]) _secondsPerFrame = _config["minutesperframe"].as<int>() *  60;
    if (_config["secondsperframe"]) _secondsPerFrame = _config["secondsperframe"].as<int>();

    // Configure font
    _font = "dejavu/DejaVuSans.ttf";
    if (_config["font"]) _font = _config["font"].as<std::string>();
    _fontSize = 8;
    if (_config["fontsize"]) _fontSize = _config["fontsize"].as<int>();

    // Configure db range
    _dBmin = -30;
    _dBmax = 0;
    if (_config["dBmin"]) _dBmin = _config["dBmin"].as<int>();
    if (_config["dBmax"]) _dBmax = _config["dBmax"].as<int>();
    _dBdelta = _dBmax - _dBmin;

    // Optionally don't align frames
    _alignFrame = true;
    if (_config["noalign"]) {
        std::string s = _config["noalign"].as<std::string>();
        if ((s.compare("true") == 0) || (s.compare("yes") == 0)) _alignFrame = false;
        else if ((s.compare("false") == 0) || (s.compare("no") == 0)) _alignFrame = true;
        else throw std::runtime_error("QGImage::configure: illegal value for noalign");
    }

    // Sync frames on time can be enabled/disabled, unless started given, where it is forced to disabled
    _syncFrames = true;
    if (_config["sync"])  _syncFrames = _config["sync"].as<bool>();

    // Configure start time
    if (_config["started"]) {
        using namespace std::chrono;

        std::tm tm = {};
        std::stringstream ss(_config["started"].as<std::string>());

        // Don't attempt to resync on frame start if start time given. Usually start time is only used when processing offline data.
        _syncFrames = false;

        //ss >> std::get_time(&tm, "%Y-%m-%dT%H:%M:%SZ"); // std::get_time() not supported on Travis' Ubuntu 14.04 build system
        std::string c; // to hold separators
        ss >> std::setw(4) >> tm.tm_year >> std::setw(1) >> c >> std::setw(2) >> tm.tm_mon >> std::setw(1) >> c >> std::setw(2) >> tm.tm_mday >> std::setw(1) >> c
        >> std::setw(2) >> tm.tm_hour >> std::setw(1) >> c >> std::setw(2) >> tm.tm_min >> std::setw(1) >> c >> std::setw(2) >> tm.tm_sec >> std::setw(1) >> c;
        tm.tm_year -= 1900; // Fix to years since 1900s
        tm.tm_mon -= 1; // Fix to 0-11 range

        // mktime takes time in local time. Should be taken in UTC, patched below
        _started = duration_cast<milliseconds>(system_clock::from_time_t(std::mktime(&tm)).time_since_epoch());

        // Calculate difference from UTC and local winter time to fix _started
        time_t t0 = time(nullptr);

        // Fix started time to be in UTC
        _started += seconds(mktime(localtime(&t0)) - mktime(gmtime(&t0)));
    }

    _levelMeter = false;
    if (_config["levelmeter"]) _levelMeter = _config["levelmeter"].as<bool>();

    // Scope size and range arenot configurable yet
    _scopeSize = 100;
    _scopeRange = 100;
    _dBK = (float)_scopeSize / _scopeRange;

    // Not yet configurable values
    _markerSize = ceil(1.2 * _fontSize);
    _borderSize = _markerSize / 2;
}

QGImage::~QGImage() {
    _pushFrame(false, true);
    _free();
}

void QGImage::addLine(const std::complex<float> *fft) {
    if (_currentLine < 0) {
        _currentLine++;
        return;
    }

    int whiteA = gdTrueColorAlpha(255, 255, 255, 125);

    // Draw a data line DC centered
    float last;
    float avg = 0;

    for (int i = _fMin; i < _fMax; i++) {
        // TODO: evaluate to do this in fft class once, for multi-image support
        float v = 10 * log10(abs(fft[(i + _fftSize) % _fftSize]) / _fftSize); // Current value, DC centered
        if (std::isnan(v)) continue;

        avg += v;

        if (v > _maxDBLevel) {
            _maxDBLevel = v;
        }

        // Truncate to -100 for spectrogram.
        // For waterfall it is anyway truncated to _dBmin
        if (v < -100.) v = -100.;

        switch (_orientation) {
            case Orientation::Horizontal:
                // TODO precalculate position of waterfall ans scope in canvas. All values in loop could be precalculated, just i, v or last added or substracted in loop
                gdImageSetPixel(
                    _im,
                    _borderSize + _freqLabelWidth + _markerSize + _currentLine,
                    _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - i, // -1 is to compensate the + _fDelta which goes one pixel after the waterfall zone
                    _db2Color(v)
                );
                if (i != _fMin)
                    gdImageLine(
                        _im,
                        _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - last * _dBK,
                        _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - i + 1, // -1 as before, + 1 to have last position
                        _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - v * _dBK,
                        _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - i,
                        whiteA
                    );
                break;

            case Orientation::Vertical:
                gdImageSetPixel(
                    _im,
                    _borderSize + _timeLabelWidth + _markerSize - _fMin + i,
                    _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _currentLine,
                    _db2Color(v)
                );
                if (i != _fMin)
                    gdImageLine(
                        _im,
                        _borderSize + _timeLabelWidth + _markerSize - _fMin + i - 1, // -1 to have last position
                        _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - last * _dBK,
                        _borderSize + _timeLabelWidth + _markerSize - _fMin + i,
                        _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - v * _dBK,
                        whiteA
                    );
                break;
        }

            last = v;
    }

    if (_levelMeter) {
        std::cout << std::fixed << std::setprecision(2) << std::setw(6) << avg / _fDelta << " dB " << QGUtils::levelBar(avg / _fDelta) << "\r" << std::flush;
    }

    _currentLine++;

    if (_currentLine >= _size) _pushFrame();
}

// Protected
// Override
void QGImage::_configureNewSource(std::shared_ptr<QGPlugin> const & source) {
    auto processor = std::dynamic_pointer_cast<QGProcessor>(source);

    auto sources = processor->sources();
    if (sources.size() == 0) {
        throw std::runtime_error("QGImage::configure: processor has no source");
    }
    auto input = std::dynamic_pointer_cast<QGInputDevice>(processor->sourcePlugin(sources[0]));

    _inputType = input->type();
    _inputSampleRate = input->sampleRate();
    _baseFreqCorrected = input->centerFreq();
    _dispFreq = input->dispFreq();
    _sampleRate = processor->sampleRate();
    _fftSize = processor->fftSize();
    _fftOverlap = processor->fftOverlap();

    // Freq/time constants used for mapping freq/time to pixel
    _freqK = (float)(_fftSize) / (_sampleRate); // pixels = freq * _freqK
    _timeK = (float)(_sampleRate) / (_fftSize - _fftOverlap); // pixels = seconds * _timeK

    // Configure and calculate size
    _size = _secondsPerFrame * _timeK;

    // Configure freq range, default value to full range of positive and negative
    _fMin = -_fftSize / 2;
    _fMax = _fftSize / 2;

    if (_config["freqmin"]) {
        auto f = _config["freqmin"].as<double>();

        if (std::abs(f) <= _sampleRate / 2) {
            // TODO !!! Check correction.
            _fMin = std::floor(((f - _baseFreqCorrected + _dispFreq) * _fftSize) / _sampleRate);
        } else if (std::abs(f - _baseFreqCorrected) <= _sampleRate / 2) {
            _fMin = std::floor(((f - _baseFreqCorrected) * _fftSize) / _sampleRate);
        } else {
            throw std::runtime_error("QGImage::configure: freqmin out of range");
        }
    }

    if (_config["freqmax"]) {
        auto f = _config["freqmax"].as<double>();

        if (std::abs(f) <= _sampleRate / 2) {
            // TODO !!! Check correction.
            _fMax = std::floor(((f - _baseFreqCorrected + _dispFreq) * _fftSize) / _sampleRate);
        } else if (std::abs(f - _baseFreqCorrected) <= _sampleRate / 2) {
            _fMax = std::floor(((f - _baseFreqCorrected) * _fftSize) / _sampleRate);
        } else {
            throw std::runtime_error("QGImage::configure: freqmax out of range");
        }
    }

    if (_fMin >= _fMax) {
        std::runtime_error("QGImage::configure: freqmin must be lower than freqmax");
    }

    _fDelta = _fMax - _fMin;

    // Configure header zone
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(0) << _dispFreq;
    _title = "QrssPiG grab on " + ss.str() + "\u202fHz";
    if (_config["header"]) {
        YAML::Node header = _config["header"];
        if (header["title"]) _title = header["title"].as<std::string>();
        if (header["callsign"]) _callsign = header["callsign"].as<std::string>();
        if (header["qth"]) _qth = header["qth"].as<std::string>();
        if (header["receiver"]) _receiver = header["receiver"].as<std::string>();
        if (header["antenna"]) _antenna = header["antenna"].as<std::string>();
    }

    _init();

    _drawDbScale(); // draw first as still overlaps with freq scales
    _drawFreqScale();

    _new(false);
}

// Private members
void QGImage::_init() {
    int black = gdTrueColor(0, 0, 0); // Not that useful, but once we want to set another background color the code is already here

    // Calculate max bounding boxes for labels, check font existence on first call
    int brect[8];

    std::stringstream ss;
    ss << QRSSPIG_NAME << " v" << QRSSPIG_VERSION_MAJOR << "." << QRSSPIG_VERSION_MINOR << "." << QRSSPIG_VERSION_PATCH;
    _qrsspigString = ss.str();
    char * err = gdImageStringFT(nullptr, brect, 0, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>(_qrsspigString.c_str()));
    if (err) throw std::runtime_error(err);
    _qrsspigLabelWidth = brect[2] - brect[0];
    _qrsspigLabelHeight = brect[1] - brect[7];

    gdImageStringFT(nullptr, brect, 0, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>("000000000\u202fHz"));
    _freqLabelWidth = brect[2] - brect[0];
    _freqLabelHeight = brect[1] - brect[7];

    gdImageStringFT(nullptr, brect, 0, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>("-100\u202fdB"));
    _dBLabelWidth = brect[2] - brect[0];
    _dBLabelHeight = brect[1] - brect[7];

    gdImageStringFT(nullptr, brect, 0, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>("00:00:00"));
    _timeLabelWidth = brect[2] - brect[0];
    _timeLabelHeight = brect[1] - brect[7];

    _computeTitleHeight();
    _computeFreqScale();
    _computeDbScale();
    _computeTimeScale();

    // Allocate canvas
    switch (_orientation) {
        case Orientation::Horizontal:
            _im = gdImageCreateTrueColor(
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize + _borderSize,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize + _timeLabelHeight + _borderSize
            );
            gdImageFilledRectangle(
                _im, 0, 0,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize + _borderSize,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize + _timeLabelHeight + _borderSize - 1,
                black
            );
            break;

        case Orientation::Vertical:
            _im = gdImageCreateTrueColor(
                _borderSize + _timeLabelWidth + _markerSize + _fDelta + _borderSize,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + _scopeSize + _borderSize
            );
            gdImageFilledRectangle(
                _im, 0, 0,
                _borderSize + _timeLabelWidth + _markerSize + _fDelta + _borderSize,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + _scopeSize + _borderSize - 1,
                black
            );
            break;
    }

    _currentLine = 0;
}

void QGImage::_new(bool incrementTime) {
    int black = gdTrueColor(0, 0, 0);

    switch (_orientation) {
        case Orientation::Horizontal:
            gdImageFilledRectangle(
                _im,
                _borderSize + _freqLabelWidth + _markerSize,
                _borderSize + _titleHeight + _markerSize,
                _borderSize + _freqLabelWidth + _markerSize + _size - 1,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1,
                black
            );
            gdImageFilledRectangle(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth,
                _borderSize + _titleHeight + _markerSize,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize - 1,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1,
                black
            );
            break;

        case Orientation::Vertical:
            gdImageFilledRectangle(
                _im,
                _borderSize + _timeLabelWidth + _markerSize,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize,
                _borderSize + _timeLabelWidth + _markerSize + _fDelta - 1,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size - 1,
                black
            );
            gdImageFilledRectangle(
                _im,
                _borderSize + _timeLabelWidth + _markerSize,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight,
                _borderSize + _timeLabelWidth + _markerSize + _fDelta - 1,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + _scopeSize - 1,
                black
            );
            break;
    }

    using namespace std::chrono;

    bool backSync = false; // Syncing can bring the current line into the past regarding new current frame, better said the new frame was created to early
    if (incrementTime) _started += seconds(_secondsPerFrame);

    if (_syncFrames) {
        milliseconds s = _started;
        _started = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        backSync = (_started < s) ? true : false;
    }

    if (_runningSince == milliseconds(0)) _runningSince = _started;

    if (_alignFrame) {
        _startedIntoFrame = _started % seconds(_secondsPerFrame); // Time into frame to align frames on correct boundary
        if (backSync) _startedIntoFrame -= seconds(_secondsPerFrame); // Negative value
        _started -= _startedIntoFrame;
        _currentLine = (_startedIntoFrame.count() * _timeK) / 1000;
    } else {
        _startedIntoFrame = milliseconds(0);
        _currentLine = 0;
    }

    _renderTitle();
    _drawTimeScale();

    _maxDBLevel = -100;
}

void QGImage::_free() {
    if (_im) gdImageDestroy(_im);
    _im = nullptr;
}

void QGImage::_computeTitleHeight() {
    using namespace std::literals::string_literals;

    _addSubTitleField("Frame start YYYY-MM-DDT00:00:00Z Running since: YYYY-MM-DDT00:00:00Z");

    _subtitles.push_back(""); // Force new line
    if (_callsign.length()) _addSubTitleField("Callsign: "s + _callsign);
    if (_qth.length()) _addSubTitleField("QTH: "s + _qth);
    if (_receiver.length()) _addSubTitleField("Receiver: "s + _receiver);
    if (_antenna.length()) _addSubTitleField("Antenna: "s + _antenna);

    _subtitles.push_back(""); // Force new line TODO: check if last line not empty
    if (_inputType.length()) _addSubTitleField("Input type: "s + _inputType);

    std::ostringstream ss;
    ss << std::fixed << std::setprecision(0) << _dispFreq;
    _addSubTitleField("Base frequency: "s + ss.str() + "\u202fHz"s);
    _addSubTitleField("Input sample rate: "s + std::to_string(_inputSampleRate) + "\u202fS/s"s);
    _addSubTitleField("Processing sample rate: "s + std::to_string(_sampleRate) + "\u202fS/s"s, true);
    _addSubTitleField("FFT size: "s + std::to_string(_fftSize));
    _addSubTitleField("FFT overlap: "s + std::to_string(_fftOverlap));
    _addSubTitleField("Time res: "s + std::to_string(1./_timeK) + "\u202fs/px"s);
    _addSubTitleField("Freq res: "s + std::to_string(1./_freqK) + "\u202fHz/px"s);

    _titleHeight = ((4 + _subtitles.size()) * _fontSize * 10) / 7; // 10/7 is interline. 4 interline for double size title
}

void QGImage::_addSubTitleField(std::string field, bool newline) {
    if (_subtitles.size() == 0) {
        _subtitles.push_back(field);
        return;
    }

    // Force new line only if last line not empty, not to make multiple empty newlines
    if (newline && _subtitles.at(_subtitles.size() - 1).length()) {
        _subtitles.push_back(field);
        return;
    }

    std::string line = _subtitles.at(_subtitles.size() - 1) + (_subtitles.at(_subtitles.size() - 1).length() ? " " : "") + field;

    int brect[8];
    gdImageStringFT(nullptr, brect, 0, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>(line.c_str()));

    int w;
    if (_orientation == Orientation::Horizontal) {
        w = _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize;
    } else {
        w = _timeLabelWidth + _markerSize + _fDelta;
    }

    if ((brect[2] - brect[0]) <= w) _subtitles.at(_subtitles.size() - 1) = line;
    else _subtitles.push_back(field);
}

void QGImage::_renderTitle() {
    int black = gdTrueColor(0, 0, 0);
    int white = gdTrueColor(255, 255, 255);

    // Clear zone
    if (_orientation == Orientation::Horizontal) {
        gdImageFilledRectangle(
            _im,
            _borderSize,
            _borderSize,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize - 1,
            _borderSize + _titleHeight - 1,
            black
        );
    } else {
        gdImageFilledRectangle(
            _im,
            _borderSize,
            _borderSize,
            _borderSize + _timeLabelWidth + _markerSize + _fDelta - 1,
            _borderSize + _titleHeight - 1,
            black
        );
    }

    // Update date in header
    char s1[128], s2[128];
    time_t t = std::chrono::duration_cast<std::chrono::seconds>(_started).count();
    std::tm *tm = std::gmtime(&t);
    strftime(s1, 128, "%FT%TZ", tm);
    t = std::chrono::duration_cast<std::chrono::seconds>(_runningSince).count();
    tm = std::gmtime(&t);
    strftime(s2, 128, "%FT%TZ", tm);
    _subtitles.at(0) = std::string("Frame start ") + s1 + " Running since: " + s2;

    int brect[8];
    int margin = (_fontSize * 5) / 7; // half interline
    int lineCursor = (_fontSize * 50) / 14; // 2.5 lines * 10/7 interline, half line margin and first line is double height

    gdImageStringFT(_im, brect, white, const_cast<char *>(_font.c_str()), 2*_fontSize, 0,
    _borderSize + margin, _borderSize + lineCursor,
    const_cast<char *>(_title.c_str()));

    if (_orientation == Orientation::Horizontal) {
        gdImageStringFT(_im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
        _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize - _qrsspigLabelWidth,
        _borderSize + lineCursor,
        const_cast<char *>(_qrsspigString.c_str()));
    } else {
        gdImageStringFT(_im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
        _borderSize + _timeLabelWidth + _markerSize + _fDelta - _qrsspigLabelWidth,
        _borderSize + lineCursor,
        const_cast<char *>(_qrsspigString.c_str()));
    }

    lineCursor += (2*_fontSize * 10) / 7;

    for (auto s: _subtitles) {
        gdImageStringFT(_im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
        _borderSize + margin, _borderSize + lineCursor,
        const_cast<char *>(s.c_str()));
        lineCursor += (_fontSize * 10) / 7;
    }
}

void QGImage::_computeFreqScale() {
    // Calculate label spacing
    int maxLabelSize;
    if (_orientation == Orientation::Horizontal) {
        maxLabelSize = 3 * _freqLabelHeight;
    } else {
        maxLabelSize = (6 * _freqLabelWidth) / 5;
    }

    // Search finest label interval withoug overlap
    if (1 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 1; _freqLabelDivs = 10; }
    else if (2 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 2; _freqLabelDivs = 2; }
    else if (5 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 5; _freqLabelDivs = 5; }
    else if (10 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 10; _freqLabelDivs = 10; }
    else if (25 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 25; _freqLabelDivs = 5; }
    else if (50 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 50; _freqLabelDivs = 10; }
    else if (100 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 100; _freqLabelDivs = 10; }
    else if (250 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 250; _freqLabelDivs = 5; }
    else if (500 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 500; _freqLabelDivs = 10; }
    else if (1000 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 1000; _freqLabelDivs = 10; }
    else if (2500 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 2500; _freqLabelDivs = 5; }
    else if (5000 * _freqK > maxLabelSize) { _hertzPerFreqLabel = 5000; _freqLabelDivs = 10; }
    else { _hertzPerFreqLabel = 10000; _freqLabelDivs = 10; }
}

void QGImage::_drawFreqScale() {
    int black = gdTrueColor(0, 0, 0);
    int white = gdTrueColor(255, 255, 255);

    // Clear zones
    if (_orientation == Orientation::Horizontal) {
        gdImageFilledRectangle(
            _im,
            _borderSize,
            _borderSize + _titleHeight,
            _borderSize + _freqLabelWidth - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth,
            _borderSize + _titleHeight + _markerSize,
            _borderSize + _freqLabelWidth + _markerSize - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth + _markerSize + _size,
            _borderSize + _titleHeight + _markerSize,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize,
            _borderSize + _titleHeight,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - 1,
            black
        );
    } else {
        gdImageFilledRectangle(
            _im,
            _borderSize + _timeLabelWidth + _markerSize - _freqLabelWidth/2,
            _borderSize + _titleHeight,
            _borderSize + _timeLabelWidth + _markerSize + _fDelta + _freqLabelWidth/2 - 1,
            _borderSize + _titleHeight + _freqLabelHeight - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _timeLabelWidth + _markerSize,
            _borderSize + _titleHeight + _freqLabelHeight,
            _borderSize + _timeLabelWidth + _markerSize + _fDelta - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _timeLabelWidth + _markerSize,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size,
            _borderSize + _timeLabelWidth + _markerSize + _fDelta - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _timeLabelWidth + _markerSize - _freqLabelWidth/2,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize,
            _borderSize + _timeLabelWidth + _markerSize + _fDelta + _freqLabelWidth/2 - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - 1,
            black
        );
    }

    // Label markers
    int fStart = std::ceil((_baseFreqCorrected + _fMin / _freqK) / _hertzPerFreqLabel) * _hertzPerFreqLabel;
    int fStop = std::floor((_baseFreqCorrected + _fMax / _freqK) / _hertzPerFreqLabel) * _hertzPerFreqLabel;

    for (int f = fStart; f <= fStop; f += _hertzPerFreqLabel) {
        int p = (f - _baseFreqCorrected) * _freqK; // Position of label

        std::ostringstream ss;
        ss << std::fixed << std::setprecision(0) << f << "\u202fHz";

        // Calculate text's bounding box
        int brect[8];
        gdImageStringFT(nullptr, brect, 0, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>(ss.str().c_str()));

        // Cache key data as they will be overriden when rendering first string
        int y = brect[1], w = brect[2] - brect[0], h = brect[1] - brect[7];

        if (_orientation == Orientation::Horizontal) {
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _freqLabelWidth - w,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p - y + .5 * h, // -1 to compensate + fDelta which goes one too far
                const_cast<char *>(ss.str().c_str())
            );
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                _borderSize + _freqLabelWidth + _markerSize - 1,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                white
            );
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + _size,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize - 1,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                white
            );
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p - y + .5 * h, // -1 to compensate + fDelta which goes one too far
                const_cast<char *>(ss.str().c_str())
            );
        } else {
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p - w/2,
                _borderSize + _titleHeight + _freqLabelHeight - y,
                const_cast<char *>(ss.str().c_str())
            );
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize - 1,
                white
            );
            gdImageLine(_im,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize - 1,
                white
            );
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p - w/2,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - y,
                const_cast<char *>(ss.str().c_str())
            );
        }
    }

    // Small tick markers
    fStart = std::ceil((_baseFreqCorrected + _fMin / _freqK) / (_hertzPerFreqLabel / _freqLabelDivs)) * (_hertzPerFreqLabel / _freqLabelDivs);
    fStop = std::floor((_baseFreqCorrected + _fMax / _freqK) / (_hertzPerFreqLabel / _freqLabelDivs)) * (_hertzPerFreqLabel / _freqLabelDivs);

    for (int f = fStart; f <= fStop; f += _hertzPerFreqLabel / _freqLabelDivs) {
        int p = (f - _baseFreqCorrected) * _freqK; // Position of marker

        if (_orientation == Orientation::Horizontal) {
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize/2,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                _borderSize + _freqLabelWidth + _markerSize - 1,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                white
            );
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + _size,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize/2 - 1,
                _borderSize + _titleHeight + _markerSize + _fDelta - 1 + _fMin - p, // -1 to compensate + fDelta which goes one too far
                white
            );
        } else {
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize/2,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize - 1,
                white
            );
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size,
                _borderSize + _timeLabelWidth + _markerSize - _fMin + p,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize/2 - 1,
                white
            );
        }
    }
}

void QGImage::_computeDbScale() {
    // Calculate lagel spacing
    int maxLabelSize;
    if (_orientation == Orientation::Horizontal) {
        maxLabelSize = (6 * _dBLabelWidth) / 5;
    } else {
        maxLabelSize = 3 * _dBLabelHeight;
    }

    // Search finest label interval without overlap
    if (1 * _dBK > maxLabelSize) { _dBPerDbLabel = 1; _dBLabelDivs = 10; }
    else if (2 * _dBK > maxLabelSize) { _dBPerDbLabel = 2; _dBLabelDivs = 2; }
    else if (5 * _dBK > maxLabelSize) { _dBPerDbLabel = 5; _dBLabelDivs = 5; }
    else if (10 * _dBK > maxLabelSize) { _dBPerDbLabel = 10; _dBLabelDivs = 10; }
    else if (20 * _dBK > maxLabelSize) { _dBPerDbLabel = 20; _dBLabelDivs = 2; }
    else if (30 * _dBK > maxLabelSize) { _dBPerDbLabel = 30; _dBLabelDivs = 3; }
    else if (40 * _dBK > maxLabelSize) { _dBPerDbLabel = 40; _dBLabelDivs = 4; }
    else if (50 * _dBK > maxLabelSize) { _dBPerDbLabel = 50; _dBLabelDivs = 5; }
    else if (60 * _dBK > maxLabelSize) { _dBPerDbLabel = 60; _dBLabelDivs = 6; }
    else if (70 * _dBK > maxLabelSize) { _dBPerDbLabel = 70; _dBLabelDivs = 7; }
    else if (80 * _dBK > maxLabelSize) { _dBPerDbLabel = 80; _dBLabelDivs = 80; }
    else if (90 * _dBK > maxLabelSize) { _dBPerDbLabel = 90; _dBLabelDivs = 90; }
    else { _dBPerDbLabel = 100; _dBLabelDivs = 10; }
}

void QGImage::_drawDbScale() {
    int black = gdTrueColor(0, 0, 0);
    int white = gdTrueColor(255, 255, 255);

    // Clear zones
    if (_orientation == Orientation::Horizontal) {
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth,
            _borderSize + _titleHeight + _markerSize + _fDelta,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - _dBLabelWidth/2,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + _scopeSize + _dBLabelWidth/2 - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize + _dBLabelHeight - 1,
            black
        );
    } else {
        gdImageFilledRectangle(
            _im,
            _borderSize,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - _dBLabelHeight/2,
            _borderSize + _timeLabelWidth - 1, // timeLabel is usually wider than dBLabel.. should take larger of them
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + _scopeSize + _dBLabelHeight/2 - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _timeLabelWidth,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight,
            _borderSize + _timeLabelWidth + _markerSize - 1, // timeLabel is usually wider.. should take larger of them
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + _scopeSize - 1,
            black
        );
    }

    // dB labels with long ticks
    for (int d = 0; d > -_scopeRange; d -= _dBPerDbLabel) {
        int l = d * _dBK; // Line number of label

        std::stringstream text;
        text << d << "\u202fdB";

        // Calculate text's bounding box
        int brect[8];
        gdImageStringFT(nullptr, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>(text.str().c_str()));

        if (_orientation == Orientation::Horizontal) {
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - l,
                _borderSize + _titleHeight + _markerSize + _fDelta,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - l,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - 1,
                white
            );
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - l - .5 * (brect[2] - brect[0]),
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - brect[7],
                const_cast<char *>(text.str().c_str())
            );
        } else {
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _timeLabelWidth - (brect[2] - brect[0]),
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - l + (brect[1] - brect[7])/2 - brect[1],
                const_cast<char *>(text.str().c_str())
            );
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth, // timeLabel is usually wider than dBLabel.. should take larger of them
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - l,
                _borderSize + _timeLabelWidth + _markerSize,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - l,
                white
            );
        }
    }

    // Small tick markers
    for (float d = 0; d > -_scopeRange; d -= (float)_dBPerDbLabel / _dBLabelDivs) {
        int l = d * _dBK; // Line number of label

        if (_orientation == Orientation::Horizontal) {
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - l,
                _borderSize + _titleHeight + _markerSize + _fDelta,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - l,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize/2 - 1,
                white
            );
        } else {
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth + _markerSize/2, // timeLabel is usually wider than dBLabel.. should take larger of them
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - l,
                _borderSize + _timeLabelWidth + _markerSize,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - l,
                white
            );
        }
    }

    // Indicator bar for dBmin-max range
    if (_orientation == Orientation::Horizontal) {
        gdImageLine(_im,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - _dBmin * _dBK,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize/4,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth - _dBmax * _dBK,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize/4,
            white);
    } else {
        gdImageLine(
            _im,
            _borderSize + _timeLabelWidth + _markerSize - _markerSize/4 - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - _dBmin * _dBK,
            _borderSize + _timeLabelWidth + _markerSize - _markerSize/4 - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight - _dBmax * _dBK,
            white
        );
    }

    // Color bar
    for (int l = 0; l < _scopeSize; l++) {
        int c = _db2Color(-(float)l/_dBK);

        if (_orientation == Orientation::Horizontal) {
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + l,
                _borderSize + _titleHeight + _markerSize + _fDelta,
                _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _freqLabelWidth + l,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize/4 - 1,
                c
            );
        } else {
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth + _markerSize - _markerSize/4,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + l,
                _borderSize + _timeLabelWidth + _markerSize - 1,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize + _freqLabelHeight + l,
                c
            );
        }
    }
}

void QGImage::_computeTimeScale() {
    // Calculate label spacing
    int maxLabelSize;
    if (_orientation == Orientation::Horizontal) {
        maxLabelSize = (6 * _timeLabelWidth) / 5;
    } else {
        maxLabelSize = 3 * _timeLabelHeight;
    }

    // Search finest label interval without overlap
    if (1 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 1; _timeLabelDivs = 10; }
    else if (2 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 2; _timeLabelDivs = 2; }
    else if (5 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 5; _timeLabelDivs = 5; }
    else if (10 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 10; _timeLabelDivs = 10; }
    else if (15 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 15; _timeLabelDivs = 3; }
    else if (30 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 30; _timeLabelDivs = 3; }
    else if (60 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 60; _timeLabelDivs = 6; }
    else if (120 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 120; _timeLabelDivs = 2; }
    else if (300 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 300; _timeLabelDivs = 5; }
    else if (600 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 600; _timeLabelDivs = 10; }
    else if (900 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 900; _timeLabelDivs = 3; }
    else if (1800 * _timeK > maxLabelSize) { _secondsPerTimeLabel = 1800; _timeLabelDivs = 3; }
    else { _secondsPerTimeLabel = 3600; _timeLabelDivs = 6; }
}

void QGImage::_drawTimeScale() {
    int black = gdTrueColor(0, 0, 0);
    int white = gdTrueColor(255, 255, 255);

    // Clear zones
    if (_orientation == Orientation::Horizontal) {
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth + _markerSize,
            _borderSize + _titleHeight + _markerSize + _fDelta,
            _borderSize + _freqLabelWidth + _markerSize + _size - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _freqLabelWidth - _timeLabelWidth/2,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize,
            _borderSize + _freqLabelWidth + _markerSize + _size + _markerSize + _timeLabelWidth/2 - 1,
            _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize + _timeLabelHeight - 1,
            black
        );
    } else {
        gdImageFilledRectangle(
            _im,
            _borderSize,
            _borderSize + _titleHeight + _freqLabelHeight,
            _borderSize + _timeLabelWidth - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size + _markerSize - 1,
            black
        );
        gdImageFilledRectangle(
            _im,
            _borderSize + _timeLabelWidth,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize,
            _borderSize + _timeLabelWidth + _markerSize - 1,
            _borderSize + _titleHeight + _freqLabelHeight + _markerSize + _size - 1,
            black
        );
    }

    int t0 = 0;

    // Time labels with long ticks

    // Realign labels on non-aligned frames
    if (_alignFrame == false) {
        t0 = _secondsPerTimeLabel - _startedIntoFrame.count() / 1000;
        while (t0 < 0) t0 += _secondsPerTimeLabel;
    }

    for (int t = t0; t <= _secondsPerFrame; t += _secondsPerTimeLabel) {
        int l = t * _timeK; // Line number of label

        std::chrono::milliseconds ms = _started + std::chrono::seconds(t);
        int hh = std::chrono::duration_cast<std::chrono::hours>(ms).count() % 24;
        int mm = std::chrono::duration_cast<std::chrono::minutes>(ms).count() % 60;
        int ss = std::chrono::duration_cast<std::chrono::seconds>(ms).count() % 60;
        std::stringstream s;
        s << std::setfill('0') << std::setw(2) << hh << ":" << std::setw(2) << mm << ":" << std::setw(2) << ss;

        // Calculatre text's bounding box
        int brect[8];
        gdImageStringFT(nullptr, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0, 0, 0, const_cast<char *>(s.str().c_str()));

        if (_orientation == Orientation::Horizontal) {
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + l,
                _borderSize + _titleHeight + _markerSize + _fDelta,
                _borderSize + _freqLabelWidth + _markerSize + l,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize - 1,
                white
            );
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _freqLabelWidth + _markerSize + l - .5 * (brect[2] - brect[0]),
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize + brect[1] - brect[7],
                const_cast<char *>(s.str().c_str())
            );
        } else {
            gdImageStringFT(
                _im, brect, white, const_cast<char *>(_font.c_str()), _fontSize, 0,
                _borderSize + _timeLabelWidth - (brect[2] - brect[0]),
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + l - .5 * (brect[1] + brect[7]) - brect[1],
                const_cast<char *>(s.str().c_str())
            );
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + l,
                _borderSize + _timeLabelWidth + _markerSize - 1,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + l,
                white
            );
        }
    }

    // Small tick markers

    // Realign ticks on non-aligned frames
    if (_alignFrame == false) {
        t0 = _secondsPerTimeLabel / _timeLabelDivs - _startedIntoFrame.count() / 1000;
        while (t0 < 0) t0 += _secondsPerTimeLabel / _timeLabelDivs;
    }

    for (float t = t0; t <= _secondsPerFrame; t += (float)_secondsPerTimeLabel / _timeLabelDivs) {
        int l = t * _timeK; // Line number of label

        if (_orientation == Orientation::Horizontal) {
            gdImageLine(
                _im,
                _borderSize + _freqLabelWidth + _markerSize + l,
                _borderSize + _titleHeight + _markerSize + _fDelta,
                _borderSize + _freqLabelWidth + _markerSize + l,
                _borderSize + _titleHeight + _markerSize + _fDelta + _markerSize/2 - 1,
                white
            );
        } else {
            gdImageLine(
                _im,
                _borderSize + _timeLabelWidth + _markerSize/2,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + l,
                _borderSize + _timeLabelWidth + _markerSize - 1,
                _borderSize + _titleHeight + _freqLabelHeight + _markerSize + l,
                white
            );
        }
    }
}

int QGImage::_db2Color(float v) {
    return _colorMap((v - _dBmin) / _dBdelta);
}

void QGImage::_pushFrame(bool intermediate, bool wait) {
    std::unique_ptr<char[], _imBufferDeleter> imBuffer;
    int frameSize;

    // _imBuffer is usually constant, but frameSize changes
    // _imBuffer might change in case of realloc, so don't cache its value
    switch (_format) {
        case Format::PNG:
            imBuffer.reset((char *)gdImagePngPtr(_im, &frameSize));
            break;
        case Format::JPG:
            imBuffer.reset((char *)gdImageJpegPtr(_im, &frameSize, -1));
            break;
    }

    auto data = std::make_shared<QGImageFileData>(
        std::move(imBuffer), frameSize, _fileNameExt, _dispFreq, _started
    );

    std::cout << "New frame ready, max level: " << _maxDBLevel << std::endl;

    for (auto const& connection: this->connections()) {
        auto config = connection.getConfig();

        // If threshold specified on connection check and skip if below
        if (config.count("threshold") == 1) {
            if (_maxDBLevel < std::stod(config.at("threshold"))) {
                std::cout << "Threshold not reached for connection " << connection.getName() << std::endl;
                continue;
            }
        }

        upload(connection, data, _fileNameTmpl, intermediate, wait);
    }

    if (!intermediate) {
        _new();
    }
}
