#pragma once

#include <array>
#include <cmath>
#include <iostream>

#include <gd.h>

#include "ColorMaps/Argos.h"
#include "ColorMaps/Blues.h"
#include "ColorMaps/Greens.h"
#include "ColorMaps/Inferno.h"
#include "ColorMaps/Lopora.h"
#include "ColorMaps/Magma.h"
#include "ColorMaps/Plasma.h"
#include "ColorMaps/Turbo.h"
#include "ColorMaps/Viridis.h"

class ColorMap {
public:
    enum class Map { QrssPiG, Grayscale, GrayscaleR, Argos, Blues, Greens, Inferno, Lopora, Magma, Plasma, Turbo, Viridis };

    explicit ColorMap(Map map = Map::QrssPiG) {
        setMap(map);
    };

    void setMap(Map map) {
        switch (map) {
            case Map::QrssPiG:
                _genMapQrssPiG();
                break;
            case Map::Grayscale:
                _genMapGrayscale();
                break;
            case Map::GrayscaleR:
                _genMapGrayscaleR();
                break;
            case Map::Argos:
                _genMap(ColorMaps::Argos::map);
                break;
            case Map::Blues:
                _genMap(ColorMaps::Blues::map);
                break;
            case Map::Greens:
                _genMap(ColorMaps::Greens::map);
                break;
            case Map::Inferno:
                _genMap(ColorMaps::Inferno::map);
                break;
            case Map::Lopora:
                _genMap(ColorMaps::Lopora::map);
                break;
            case Map::Magma:
                _genMap(ColorMaps::Magma::map);
                break;
            case Map::Plasma:
                _genMap(ColorMaps::Plasma::map);
                break;
            case Map::Turbo:
                _genMap(ColorMaps::Turbo::map);
                break;
            case Map::Viridis:
                _genMap(ColorMaps::Viridis::map);
                break;
        }
    }

    // value: [0..1]
    int operator()(float value) {
        // Truncate to [0..1] range
        if (value < 0) value = 0;
        if (value > 1) value = 1;

        return _color_map[(int)trunc((_map_size - 1) * value)];
    }


private:
    static int const _map_size{256};
    std::array<int, _map_size> _color_map;

    void _genMapQrssPiG() {
        int ii = 0;
        for (int i = 0; i <= 255; i+= 4) _color_map[ii++] = gdTrueColor(0, 0, i);         // Black -> Blue
        for (int i = 0; i <= 255; i+= 4) _color_map[ii++] = gdTrueColor(0, i, 255);       // Blue -> Cyan
        for (int i = 0; i <= 255; i+= 4) _color_map[ii++] = gdTrueColor(i, 255, 255 - i); // Cyan -> Green
        for (int i = 0; i <= 255; i+= 4) _color_map[ii++] = gdTrueColor(255, 255 - i, 0); // Green -> red
    }

    void _genMapGrayscale() {
        for (int i = 0; i < _map_size; i++) _color_map[i] = gdTrueColor(i, i, i);
    }

    void _genMapGrayscaleR() {
        for (int i = 0; i < _map_size; i++) _color_map[i] = gdTrueColor(255 - i, 255 - i, 255 - i);
    }

    void _genMap(int map[]) {
        for (int i = 0; i < _map_size; i++) {
            _color_map[i] = 0x00ffffff & map[i];
        }
    }
};
