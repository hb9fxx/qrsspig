#include "QGInputLime.h"

#include <cstring>
#include <functional>
#include <iostream>
#include <stdexcept>

#define MAX_DEVICES 16

extern "C" QGInputDevice* create_object(const YAML::Node &config, int index) {
        return new QGInputLime(config, index);
}

extern "C" std::string module_info() {
        return QGInputLime::moduleInfo();
}

extern "C" std::vector<std::string> list_devices() {
        return QGInputLime::listDevices();
}

std::string QGInputLime::moduleInfo() {
	return std::string("Version ") + LMS_GetLibraryVersion();
}

std::vector<std::string> QGInputLime::listDevices() {
	std::vector<std::string> list;

	lms_info_str_t deviceList[MAX_DEVICES];
	int numDevices = LMS_GetDeviceList(deviceList);
	for (int i = 0; i < numDevices; i++) list.push_back(deviceList[i]);

	return list;
}

QGInputLime::QGInputLime(const YAML::Node &config, int index)
: QGInputDevice("limesdr", index, config), _device(nullptr) {
	_type = "LimeSdr";
	std::string deviceName;
	unsigned int channel = 0;
	size_t oversample = 0;
	int antenna = -1;
	unsigned int gain = 0;
	float_type lpf = 0.;

	if (config["device"]) deviceName = config["device"].as<std::string>();
	if (config["channel"]) channel = config["channel"].as<unsigned int>();
	if (config["oversample"]) oversample = config["oversample"].as<unsigned int>();
	if (config["antenna"]) antenna = config["antenna"].as<int>();
	if (config["gain"]) gain = config["gain"].as<int>();
	if (config["lpf"]) lpf = config["lpf"].as<double>();

  if (_noDevice) return;

	if (deviceName.size()) {
		lms_info_str_t d;
		strncpy(d, deviceName.c_str(), sizeof(d));
		if (LMS_Open(&_device, d, nullptr)) throw std::runtime_error(std::string("Error opening device ") + deviceName);
	} else {
		if (LMS_Open(&_device, nullptr, nullptr)) throw std::runtime_error("Error opening default device");
	}

	if (LMS_Init(_device)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_Init failed: ") + e);
	}

	std::cout << "Device: " << LMS_GetDeviceInfo(_device) << std::endl;

	int numRXChannels = LMS_GetNumChannels(_device, LMS_CH_RX);
	std::cout << numRXChannels << " RX channels" << std::endl;

	int numTXChannels = LMS_GetNumChannels(_device, LMS_CH_TX);
	std::cout << numTXChannels << " TX channels" << std::endl;

	if (LMS_EnableChannel(_device, LMS_CH_RX, channel, true)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_EnableChannel failed: ") + e);
	}

	// Get and show capabilities
	lms_range_t range;

	if (LMS_GetSampleRateRange(_device, LMS_CH_RX, &range)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetSampleRate failed: ") + e);
	}

	std::cout << "SR min " << range.min << " SR max " << range.max << " SR step " << range.step << std::endl;

	if (LMS_GetLOFrequencyRange(_device, LMS_CH_RX, &range)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetLOFrequencyRange failed: ") + e);
	}

	std::cout << "LOF min " << range.min << " LOF max " << range.max << " LOF step " << range.step << std::endl;

	int numAntennas;
	lms_name_t antennas[10];

	numAntennas = LMS_GetAntennaList(_device, LMS_CH_RX, channel, antennas);

	if (numAntennas < 0) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetAntennaList failed: ") + e);
	}

	std::cout << "Num antennas: " << numAntennas << std::endl;

	for (int i = 0; i < numAntennas; i++) {
		std::cout << "\t" << i << "\t" << antennas[i] << std::endl;
	}

	if (LMS_GetLPFBWRange(_device, LMS_CH_RX, &range)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetLPFBWRange failed: ") + e);
	}

	std::cout << "LPF min " << range.min << " LPF max " << range.max << " LPF step " << range.step << std::endl;

	// Set config
	if (LMS_SetSampleRate(_device, _sampleRate, oversample)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_SetSampleRate failed: ") + e);
	}

	float_type hostSr, rfSr;

	if (LMS_GetSampleRate(_device, LMS_CH_RX, channel, &hostSr, &rfSr)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetSampleRate failed: ") + e);
	}

  _sampleRate = std::round(hostSr);

	std::cout << "Samplerate host: " << hostSr << " RF: " << rfSr << " oversample: " << (rfSr/hostSr) << std::endl;

	float_type frequency = tunerFreq();

	if (LMS_SetLOFrequency(_device, LMS_CH_RX, channel, frequency)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_SetLOFrequency failed: ") + e);
	}

	if (LMS_GetLOFrequency(_device, LMS_CH_RX, channel, &frequency)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetLOFrequency failed: ") + e);
	}

  _updateTunerFreq(frequency);

	std::cout << "LO Frequency: " << frequency << std::endl;

	if (antenna >= 0) {
		if (LMS_SetAntenna(_device, LMS_CH_RX, channel, antenna)) {
			std::string e(LMS_GetLastErrorMessage());
			if (_device) LMS_Close(_device);
			throw std::runtime_error(std::string("LMS_SetAntenna failed: ") + e);
		}
	}

	antenna = LMS_GetAntenna(_device, LMS_CH_RX, channel);

	if (antenna < 0) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetAntenna failed: ") + e);
	}

	std::cout << "Antenna: " << antenna << std::endl;

	if (LMS_GetAntennaBW(_device, LMS_CH_RX, channel, antenna, &range)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetAntennaBW failed: ") + e);
	}

	std::cout << "Antenna min " << range.min << " Antenna max " << range.max << " Antenna step " << range.step << std::endl;

#ifdef HAVE_LIBLIMESUITE_SET_GAIN_DB
	if (LMS_SetGaindB(_device, LMS_CH_RX, channel, gain)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_SetGaindB failed: ") + e);
	}

	float_type g;
	if (LMS_GetNormalizedGain(_device, LMS_CH_RX, channel, &g)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetNormalizedGain failed: ") + e);
	}

	if (LMS_GetGaindB(_device, LMS_CH_RX, channel, &gain)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetGaindB failed: ") + e);
	}

	std::cout << "Gain: " << gain << " (" << g << ")" << std::endl;

#else
	if (gain != 0) {
		std::cout << "Warning:\tgain option not available for installed liblimesuite version" << std::endl;
	}
#endif // HAVE_LIBLIMESUITE_SET_GAIN_DB

	if (lpf > 0) {
		if (LMS_SetLPFBW(_device, LMS_CH_RX, channel, lpf)) {
			std::string e(LMS_GetLastErrorMessage());
			if (_device) LMS_Close(_device);
			throw std::runtime_error(std::string("LMS_SetLPFBW failed: ") + e);
		}
	}

	if (LMS_GetLPFBW(_device, LMS_CH_RX, channel, &lpf)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_GetLPFBW failed: ") + e);
	}

	std::cout << "LPF: " << lpf << std::endl;

	if (LMS_SetGFIRLPF(_device, LMS_CH_RX, channel, true, lpf)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_SetGFIRLPF failed: ") + e);
	}

	if (LMS_Calibrate(_device, LMS_CH_RX, channel, 2500000, 0)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_Calibrate failed: ") + e);
	}

	// Setup stream
	_stream.isTx = false;
	_stream.channel = channel;
	_stream.fifoSize = 1024;
	_stream.throughputVsLatency = 1.;
	_stream.dataFmt = lms_stream_t::LMS_FMT_F32;

	if (LMS_SetupStream(_device, &_stream)) {
		std::string e(LMS_GetLastErrorMessage());
		if (_device) LMS_Close(_device);
		throw std::runtime_error(std::string("LMS_SetupStream failed: ") + e);
	}
}

QGInputLime::~QGInputLime() {
	if (_device) {
    LMS_DestroyStream(_device, &_stream);
    LMS_Close(_device);
  }
}

void QGInputLime::_startDevice() {
	if (LMS_StartStream(&_stream)) {
		std::string e(LMS_GetLastErrorMessage());
		throw std::runtime_error(std::string("LMS_StartStream failed: ") + e);
	}

	_t = std::thread(std::bind(&QGInputLime::_run, this));

	return;
}

void QGInputLime::_stopDevice() {
	_t.join();

	lms_stream_status_t status;

	LMS_GetStreamStatus(&_stream, &status);

	std::cout << status.active << " " << status.fifoFilledCount << " " << status.fifoSize << std::endl;
	std::cout << status.underrun << " " << status.overrun << " " << status.droppedPackets << std::endl;
	std::cout << status.sampleRate << " " << status.linkRate << " " << status.timestamp << std::endl;

	if (LMS_StopStream(&_stream)) {
		std::string e(LMS_GetLastErrorMessage());
		throw std::runtime_error(std::string("LMS_StopStream failed: ") + e);
	}
}

void QGInputLime::_run() {
	float buffer[2048];
	lms_stream_meta_t meta;

	while (_running) {
		int received = LMS_RecvStream(&_stream, buffer, 1024, &meta, 1000);

		if (received < 0) {
			std::string e(LMS_GetLastErrorMessage());
			std::cout << "Error: " << e << std::endl;
			continue;
		}
//		std::cout << "received " << received << std::endl;

		// Drop new data if buffer full
		if (_bufferSize + received > _bufferCapacity) {
			std::cout << "drop" << std::endl;
			continue;
		}

		for (int j = 0; j < received * 2;) {
      // F32 IQ data
			float i = buffer[j++];
			float q = buffer[j++];
			_buffer[_bufferHead++] = std::complex<float>(i, q);
			_bufferHead %= _bufferCapacity;
		}

		_bufferSize += received;
	}
}
