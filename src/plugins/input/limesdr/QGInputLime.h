#pragma once

#include "QGInputDevice.h"

#include <thread>

#include <lime/LimeSuite.h>

class QGInputLime: public QGInputDevice {
public:
	static std::string moduleInfo();
	static std::vector<std::string> listDevices();

	QGInputLime(const YAML::Node &config, int index);
	~QGInputLime();

private:
	void _startDevice();
	void _stopDevice();

	void _run();

	std::thread _t;
	lms_device_t *_device;
	lms_stream_t _stream;
};
