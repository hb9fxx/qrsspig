#pragma once

#include "QGInputDevice.h"

#include <memory>
#include <thread>

class QGInputTest: public QGInputDevice {
public:
	static std::string moduleInfo();
	static std::vector<std::string> listDevices();

	QGInputTest(const YAML::Node &config, int index);
	~QGInputTest();

private:
	void _startDevice();
	void _stopDevice();

	void _process();

	double _level;
	unsigned long long int _maxSamples;

	std::thread _thread;
};
