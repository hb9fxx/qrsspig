#include "QGInputTest.h"

#include <functional>
#include <iostream>
#include <random>
#include <stdexcept>

extern "C" std::string module_info() {
        return QGInputTest::moduleInfo();
}

extern "C" std::vector<std::string> list_devices() {
        return QGInputTest::listDevices();
}

extern "C" QGInputDevice* create_object(const YAML::Node &config, int index) {
        return new QGInputTest(config, index);
}

std::string QGInputTest::moduleInfo() {
	return std::string("Test noise signal source");
}

std::vector<std::string> QGInputTest::listDevices() {
	std::vector<std::string> list;

	list.push_back("Test noise signal");

	return list;
}

QGInputTest::QGInputTest(const YAML::Node &config, int index) :
QGInputDevice("test", index, config),
_level{.001},
_maxSamples{0} {
	_type = "Test";

	if (config["level"]) {
		_level = config["level"].as<double>();
	}
	if (config["maxsamples"]) {
		_maxSamples = config["maxsamples"].as<unsigned long long int>();
	}
	if (config["maxseconds"]) {
		_maxSamples = config["maxseconds"].as<unsigned long long int>() * _sampleRate;
	}

  _noTuner();
}

QGInputTest::~QGInputTest() {
	if (_thread.joinable()) _thread.join();
}

void QGInputTest::_startDevice() {
	_thread = std::thread(std::bind(&QGInputTest::_process, this));
}

void QGInputTest::_stopDevice() {
	if (_thread.joinable()) _thread.join();
}

void QGInputTest::_process() {
	unsigned long long int samples{0};
	auto g = std::ranlux24();
	auto d = std::uniform_real_distribution<>(.0, _level);

	while (_running) {
		while (_bufferSize + 1 > _bufferCapacity) {
			std::this_thread::sleep_for(std::chrono::microseconds(1000));
		}

		_buffer[_bufferHead++] = std::complex<float>(d(g), d(g));
		_bufferHead %= _bufferCapacity;
		_bufferSize++;

		samples++;
		if (_maxSamples && samples >= _maxSamples) { break; }
	}

	// In case we came here because of maxSamples reached
	_running = false;
}
