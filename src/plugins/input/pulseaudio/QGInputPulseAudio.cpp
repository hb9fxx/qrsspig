#include "QGInputPulseAudio.h"

#include <functional>
#include <iostream>
#include <stdexcept>

extern "C" QGInputDevice* create_object(const YAML::Node &config, int index) {
    return new QGInputPulseAudio(config, index);
}

extern "C" std::string module_info() {
    return QGInputPulseAudio::moduleInfo();
}

extern "C" std::vector<std::string> list_devices() {
    return QGInputPulseAudio::listDevices();
}

class QGPulseAudioSession {
public:
    QGPulseAudioSession() : _name{"qrsspig"} {};
    explicit QGPulseAudioSession(std::string const & name) : _name{name} {};

    pa_mainloop * getMainloop() {
        if (!_mainloop) {
            std::unique_ptr<pa_mainloop, _paMainLoopDeleter> mainloop{pa_mainloop_new()};

            if (!mainloop) {
                throw std::runtime_error("Error creating PulseAudio mainloop");
            }

            _mainloop = std::move(mainloop);
        }

        return _mainloop.get();
    }

    pa_context * getContext() {
        if (!_context) {
            auto mainloop = getMainloop();
            auto mainloopApi = pa_mainloop_get_api(mainloop);

            if (!mainloopApi) {
                throw std::runtime_error("Error getting PulseAudio mainloop api");
            }

            std::unique_ptr<pa_context, _paContextDeleter> context{pa_context_new_with_proplist(mainloopApi, _name.c_str(), nullptr)};

            if (!context) {
                throw std::runtime_error("Error creating PulseAudio context");
            }

            std::atomic<int> ready{0};
            pa_context_set_state_callback(context.get(), QGPulseAudioSession::_pa_context_state_cb, &ready);
            auto connect = pa_context_connect(context.get(), nullptr, PA_CONTEXT_NOFLAGS, nullptr);

            if (connect < 0) {
                auto e = pa_strerror(pa_context_errno(context.get()));
                throw std::runtime_error(std::string("Error connecting to PulseAudio server: ") + e);
            }

            while (!ready) {
                pa_mainloop_iterate(mainloop, 1, nullptr);
            }

            if (ready < 0) {
              auto e = pa_strerror(pa_context_errno(context.get()));
              throw std::runtime_error(std::string("Error connecting to PulseAudio server: ") + e);
            }

            _context = std::move(context);
        }

        return _context.get();
    }

    pa_stream * getStream(pa_sample_spec const & ss) {
        if (!_stream) {
            auto context = getContext();

            std::unique_ptr<pa_stream, _paStreamDeleter> stream{pa_stream_new(context, _name.c_str(), &ss, nullptr)};

            if (!stream) {
                throw std::runtime_error(std::string("Error creating PulseAudio stream: ") + pa_strerror(pa_context_errno(context)));
            }

            _stream = std::move(stream);
        }

        return _stream.get();
    }

private:
    static void _pa_context_state_cb(pa_context *context, void *userdata) {
        auto * ready = static_cast<std::atomic<int>*>(userdata);
        pa_context_state_t state;

        state = pa_context_get_state(context);

        switch (state) {
        case PA_CONTEXT_READY:
            *ready = 1;
            break;

        case PA_CONTEXT_FAILED:
        case PA_CONTEXT_TERMINATED:
            *ready = -1;
            break;

        default:
            break;
        }
    }
    struct _paMainLoopDeleter {
        void operator()(pa_mainloop * mainloop) const {
            pa_mainloop_free(mainloop);
        }
    };

    struct _paContextDeleter {
        void operator()(pa_context * context) const {
            // Crash on jessie
//            pa_context_state_t state = pa_context_get_state(context);

//            if (state == PA_CONTEXT_READY) {
//                pa_context_disconnect(context);
//            }

            pa_context_unref(context);
        }
    };

    struct _paStreamDeleter {
        void operator()(pa_stream * stream) const {
            pa_stream_state_t state = pa_stream_get_state(stream);

            if (state == PA_STREAM_READY) {
                pa_stream_disconnect(stream);
            }

            pa_stream_unref(stream);
        }
    };

    std::string _name;
    std::unique_ptr<pa_mainloop, _paMainLoopDeleter> _mainloop;
    std::unique_ptr<pa_context, _paContextDeleter> _context;
    std::unique_ptr<pa_stream, _paStreamDeleter> _stream;
};

std::string QGInputPulseAudio::moduleInfo() {
    return std::string("Version ") + pa_get_library_version();
}

std::vector<std::string> QGInputPulseAudio::listDevices() {
    std::vector<std::string> list;
    QGPulseAudioSession pa_session;
    auto mainloop = pa_session.getMainloop();
    auto context = pa_session.getContext();

    pa_operation *operation = pa_context_get_source_info_list(context, QGInputPulseAudio::_pa_list_cb, &list);

    if (operation) {
      while (pa_operation_get_state(operation) != PA_OPERATION_DONE) {
        pa_mainloop_iterate(mainloop, 1, nullptr);
      }
      pa_operation_unref(operation);
    } else {
      auto e = pa_strerror(pa_context_errno(context));
      throw std::runtime_error(std::string("Error getting PulseAudio source list: ") + e);
    }

    return list;
}

QGInputPulseAudio::QGInputPulseAudio(const YAML::Node &config, int index) :
QGInputDevice("pulseaudio", index, config),
_deviceName{},
_channel{Channel::LEFT},
_createDevice{false},
_pa_mainloop{nullptr},
_paModuleIndex{0},
_ready{0} {
  _type = "PulseAudio";

  if (config["device"]) {
      _deviceName = config["device"].as<std::string>();
  }
  if (config["channel"]) {
    std::string c = config["channel"].as<std::string>();

    if (c == "mono") _channel = Channel::MONO;
    else if (c == "left") _channel = Channel::LEFT;
    else if (c == "right") _channel = Channel::RIGHT;
    else if (c == "iq") _channel = Channel::IQ;
    else if (c == "inviq") _channel = Channel::INVIQ;
    else throw std::runtime_error("YAML: channel value unrecognized");
  }
  if (config["createdevice"]) {
      _createDevice = config["createdevice"].as<bool>();
  }

  _noTuner();

  if (_noDevice) return;

  if (_createDevice) {
      // If no device name given, create "qrsspig_output"
      if (_deviceName.length() == 0) {
          _deviceName = "qrsspig_output";
      }

      _paModuleIndex = _loadDevice(_deviceName);

      // Append .monitor as we created a sink device and what to monitor it
      _deviceName += ".monitor";
  }
}

QGInputPulseAudio::~QGInputPulseAudio() {
    // Duplicate code from _stopDevice not to call virtual function in destructor
    if (_pa_mainloop) {
        pa_mainloop_quit(_pa_mainloop, 0);
    }

    if (_thread.joinable()) {
        _thread.join();
    }

    if (_paModuleIndex) {
        _unloadDevice(_paModuleIndex);
    }
}

void QGInputPulseAudio::_startDevice() {
    _thread = std::thread(std::bind(&QGInputPulseAudio::_run, this));
}

void QGInputPulseAudio::_stopDevice() {
    if (_pa_mainloop) {
        pa_mainloop_quit(_pa_mainloop, 0);
    }

    if (_thread.joinable()) {
        _thread.join();
    }
}

void QGInputPulseAudio::_run() {
    pa_sample_spec ss;
    pa_buffer_attr ba;

    switch (_channel) {
    case Channel::MONO:
        ss.channels = 1;
        break;
    case Channel::LEFT:
    case Channel::RIGHT:
    case Channel::IQ:
    case Channel::INVIQ:
        ss.channels = 2;
        break;
    }
    ss.format = PA_SAMPLE_S16LE;
    ss.rate = _sampleRate;

    ba.maxlength = (uint32_t)-1;
    ba.fragsize = (uint32_t)-1;

    QGPulseAudioSession pa_session;
    auto mainloop = pa_session.getMainloop();
    auto context = pa_session.getContext();
    auto stream = pa_session.getStream(ss);

    _ready = 0;
    pa_stream_set_state_callback(stream, QGInputPulseAudio::_pa_stream_state_cb, &_ready);
    pa_stream_set_read_callback(stream, QGInputPulseAudio::_pa_stream_read_cb, this);
    pa_stream_connect_record(stream, (_deviceName.length() ? _deviceName.c_str() : nullptr), &ba, PA_STREAM_NOFLAGS);

    while (!_ready) pa_mainloop_iterate(mainloop, 1, nullptr);

    if (_ready < 0) {
        throw std::runtime_error(std::string("Error connecting PulseAudio stream: ") + pa_strerror(pa_context_errno(context)));
    }

    _pa_mainloop = mainloop;
    pa_mainloop_run(mainloop, nullptr);
    _pa_mainloop = nullptr;
}

void QGInputPulseAudio::_process(pa_stream * stream, size_t /*nbytes*/) {
    const unsigned char* p(nullptr);
    size_t s(0);

    if (pa_stream_peek(stream, (const void **)&p, &s)) throw std::runtime_error("Error peeking data");

    while (s) {
        if (p) {
            signed short int i = 0, q = 0;

  		// Only S16_LE supported so far
  		switch (_channel) {
  		case Channel::MONO:
  			if (_bufferSize + s/2 <= _bufferCapacity) {
  				for (size_t j = 0; j < s;) {
  					i = p[j++];
  					i += p[j++] << 8;
  					_buffer[_bufferHead++] = std::complex<float>(i / 32768., q / 32768.);
  					_bufferHead %= _bufferCapacity;
  				}

  				_bufferSize += s/2;
  			} else {
  				std::cout << "drop" << std::endl;
  			}
  			break;

  		case Channel::LEFT:
  			if (_bufferSize + s/4 <= _bufferCapacity) {
  				for (size_t j = 0; j < s;) {
  					i = p[j++];
  					i += p[j++] << 8;
  					j += 2;
  					_buffer[_bufferHead++] = std::complex<float>(i / 32768., q / 32768.);
  					_bufferHead %= _bufferCapacity;
  				}

  				_bufferSize += s/4;
  			} else {
  				std::cout << "drop" << std::endl;
  			}
  			break;

  		case Channel::RIGHT:
  			if (_bufferSize + s/4 <= _bufferCapacity) {
  				for (size_t j = 0; j < s;) {
  					j += 2;
  					i = p[j++];
  					i += p[j++] << 8;
  					_buffer[_bufferHead++] = std::complex<float>(i / 32768., q / 32768.);
  					_bufferHead %= _bufferCapacity;
  				}

  				_bufferSize += s/4;
  			} else {
  				std::cout << "drop" << std::endl;
  			}
  			break;

  		case Channel::IQ:
  			if (_bufferSize + s/4 <= _bufferCapacity) {
  				for (size_t j = 0; j < s;) {
  					i = p[j++];
  					i += p[j++] << 8;
  					q = p[j++];
  					q += p[j++] << 8;
  					_buffer[_bufferHead++] = std::complex<float>(i / 32768., q / 32768.);
  					_bufferHead %= _bufferCapacity;
  				}

  				_bufferSize += s/4;
  			} else {
  				std::cout << "drop" << std::endl;
  			}
  			break;

  		case Channel::INVIQ:
  			if (_bufferSize + s/4 <= _bufferCapacity) {
  				for (size_t j = 0; j < s;) {
  					q = p[j++];
  					q += p[j++] << 8;
  					i = p[j++];
  					i += p[j++] << 8;
  					_buffer[_bufferHead++] = std::complex<float>(i / 32768., q / 32768.);
  					_bufferHead %= _bufferCapacity;
  				}

  				_bufferSize += s/4;
  			} else {
  				std::cout << "drop" << std::endl;
  			}
  			break;
  		}
    }

    pa_stream_drop(stream);

    if (pa_stream_peek(stream, (const void **)&p, &s)) throw std::runtime_error("Error peeking data");
	}
}

int QGInputPulseAudio::_loadDevice(std::string const & deviceName) {
    QGPulseAudioSession pa_session{"qrsspig_load"};
    auto mainloop = pa_session.getMainloop();
    auto context = pa_session.getContext();

    std::atomic<unsigned int> moduleIndex{0};
    auto operation = pa_context_load_module(context, "module-null-sink", (std::string("sink_name=") + deviceName).c_str(), QGInputPulseAudio::_pa_index_callback, &moduleIndex);

    if (!operation) {
        throw std::runtime_error("Error loading module");
    }

    while (!moduleIndex) {
        pa_mainloop_iterate(mainloop, 1, nullptr);
    }

    pa_operation_unref(operation);

    return moduleIndex;
}

void QGInputPulseAudio::_unloadDevice(int moduleIndex) {
    QGPulseAudioSession pa_session{"qrsspig_unload"};
    auto mainloop = pa_session.getMainloop();
    auto context = pa_session.getContext();

    std::atomic<int> ready{0};
    auto operation = pa_context_unload_module(context, moduleIndex, QGInputPulseAudio::_pa_simple_callback, &ready);

    if (!operation) {
        throw std::runtime_error("Error unloading module");
    }

    while (!ready) {
        pa_mainloop_iterate(mainloop, 1, nullptr);
    }

    pa_operation_unref(operation);
}

void QGInputPulseAudio::_pa_list_cb(pa_context */*context*/, const pa_source_info *card, int /*eol*/, void *userdata) {
    if (card) {
        auto * list = static_cast<std::vector<std::string>*>(userdata);
        list->push_back(std::string("[") + std::to_string(card->index) + "] " + card->name);
    }
}

void QGInputPulseAudio::_pa_index_callback(pa_context */*c*/, uint32_t index, void *userdata) {
    auto * deviceIndex = static_cast<std::atomic<unsigned int> *>(userdata);

    *deviceIndex = index;
}

void QGInputPulseAudio::_pa_simple_callback(pa_context */*c*/, int success, void *userdata) {
    auto * ready = static_cast<std::atomic<int> *>(userdata);

    if (success) {
        *ready = 1;
    } else {
        *ready = -1;
    }
}

void QGInputPulseAudio::_pa_stream_state_cb(pa_stream * stream, void *userdata) {
    auto * ready = static_cast<std::atomic<int>*>(userdata);
    pa_stream_state_t state;

    state = pa_stream_get_state(stream);

    switch (state) {
    case PA_STREAM_READY:
        *ready = 1;
        break;

    case PA_STREAM_FAILED:
    case PA_STREAM_TERMINATED:
    *ready = -1;
    break;

    default:
        break;
    }
}

void QGInputPulseAudio::_pa_stream_read_cb(pa_stream * stream, size_t nbytes, void *userdata) {
    static_cast<QGInputPulseAudio *>(userdata)->_process(stream, nbytes);
}
