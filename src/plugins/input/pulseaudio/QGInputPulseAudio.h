#pragma once

#include "QGInputDevice.h"

#include <atomic>
#include <thread>

#include <pulse/pulseaudio.h>

class QGInputPulseAudio: public QGInputDevice {
public:
	static std::string moduleInfo();
	static std::vector<std::string> listDevices();

	QGInputPulseAudio(const YAML::Node &config, int index);
	~QGInputPulseAudio() override;

private:
	enum class Channel { MONO, LEFT, RIGHT, IQ, INVIQ };

	void _startDevice() override;
	void _stopDevice() override;

	void _run();

	void _process(pa_stream * stream, size_t nbytes);

	static int _loadDevice(std::string const & deviceName);
	static void _unloadDevice(int moduleIndex);

	static void _pa_list_cb(pa_context *c, const pa_source_info *i, int eol, void *userdata);
	static void _pa_index_callback(pa_context *c, uint32_t index, void *userdata);
	static void _pa_simple_callback(pa_context *c, int success, void *userdata);
	static void _pa_stream_state_cb(pa_stream * stream, void *userdata);
	static void _pa_stream_read_cb(pa_stream * stream, size_t nbytes, void * userdata);

	std::string _deviceName;
	Channel _channel;
	bool _createDevice;

	pa_mainloop * _pa_mainloop; // No ownership.
	int _paModuleIndex;

	// Must be declared at class level otherwise it will be out of scope when context gets destroyed on object destruction
	std::atomic<int> _ready;

	std::thread _thread;
};
