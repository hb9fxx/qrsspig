#include "QGInputHackRF.h"

#include "Config.h"

#include <chrono>
#include <iostream>
#include <stdexcept>

extern "C" QGInputDevice* create_object(const YAML::Node &config, int index) {
        return new QGInputHackRF(config, index);
}

extern "C" std::string module_info() {
        return QGInputHackRF::moduleInfo();
}

extern "C" std::vector<std::string> list_devices() {
        return QGInputHackRF::listDevices();
}

std::string QGInputHackRF::moduleInfo() {
#ifdef HAVE_LIBHACKRF_LIBRARY_VERSION
	return std::string("Version ") + hackrf_library_version() + " release: " + hackrf_library_release();
#else
	return "";
#endif // HAVE_LIBHACKRF_LIBRARY_VERSION
}

std::vector<std::string> QGInputHackRF::listDevices() {
	std::vector<std::string> list;

#ifdef HAVE_LIBHACKRF_DEVICE_LIST
	hackrf_device* device;
	int r = hackrf_init();

	if (r != HACKRF_SUCCESS) {
		throw std::runtime_error(std::string("HackRF init returned ") + std::to_string(r));
	}

	hackrf_device_list_t *l = hackrf_device_list();

	for (int i = 0; i < l->devicecount; i++) {
		if ((r = hackrf_device_list_open(l, i, &device)) == 0) {
			unsigned char boardId;
			std::unique_ptr<char[]> version(new char[255]);
			hackrf_board_id_read(device, &boardId);
			hackrf_version_string_read(device, version.get(), 255);
			list.push_back(std::to_string(i) + ": " + hackrf_board_id_name((hackrf_board_id)boardId) + " v" + version.get() + " (serial: " + l->serial_numbers[i] + ")");
			hackrf_close(device);
		} else {
			list.push_back(std::to_string(i) + "\tError opening device: " + hackrf_error_name((hackrf_error)r));
		}
	}

	hackrf_device_list_free(l);

	hackrf_exit();
#else
	list.push_back("Device listing not supported by this libhackrf version");
#endif // HAVE_LIBHACKRF_DEVICE_LIST

	return list;
}

QGInputHackRF::QGInputHackRF(const YAML::Node &config, int index)
: QGInputDevice("hackrf", index, config), _device(nullptr) {
    _type = "HackRF";

    std::string deviceSerial{};
    int deviceIndex{-1};
    unsigned char antennaPowerEnabled{0};
    unsigned char ampEnabled{0};
    unsigned int lnaGain{16};
    unsigned int vgaGain{16};

    if (config["deviceserial"]) deviceSerial = config["deviceserial"].as<std::string>();
    if (config["deviceindex"]) deviceIndex = config["deviceindex"].as<int>();
    if (config["antennapower"]) antennaPowerEnabled = config["antennapower"].as<bool>() ? 1 : 0;
    if (config["amplifier"]) ampEnabled = config["amplifier"].as<bool>() ? 1 : 0;
    if (config["lnagain"]) lnaGain = config["lnagain"].as<unsigned int>();
    if (config["vgagain"]) vgaGain = config["vgagain"].as<unsigned int>();

    if (deviceSerial.length() > 0 && deviceIndex != -1) {
        throw std::runtime_error(std::string("HackRF: deviceserial and deviceindex are mutually exclusive"));
    }

    if (lnaGain > 40) lnaGain = 40;
    lnaGain &= 0x38;
    if (vgaGain > 62) vgaGain = 62;
    vgaGain &= 0x3e;

    if (_noDevice) return;

    int r = hackrf_init();

    if (r != HACKRF_SUCCESS) {
        throw std::runtime_error(std::string("HackRF init returned ") + std::to_string(r));
    }

    if (deviceSerial.length() > 0) {
    #ifdef HAVE_LIBHACKRF_DEVICE_LIST
        r = hackrf_open_by_serial(deviceSerial.c_str(), &_device);
    #else
        hackrf_exit();
      	throw std::runtime_error("Device opening by serial not supported by this libhackrf version");
    #endif // HAVE_LIBHACKRF_DEVICE_LIST
    } else if (deviceIndex != -1)  {
    #ifdef HAVE_LIBHACKRF_DEVICE_LIST
        hackrf_device_list_t *l = hackrf_device_list();
        r = hackrf_device_list_open(l, deviceIndex, &_device);
        hackrf_device_list_free(l);
    #else
        hackrf_exit();
      	throw std::runtime_error("Device opening by index not supported by this libhackrf version");
    #endif // HAVE_LIBHACKRF_DEVICE_LIST
    } else {
        r = hackrf_open(&_device);
    }

    if (r != HACKRF_SUCCESS) {
        hackrf_exit();
        throw std::runtime_error(std::string("HackRF opening failed: ") + std::to_string(r));
    }

	r = hackrf_set_sample_rate(_device, _sampleRate);

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting samplerate failed: ") + std::to_string(r));
	}

	r = hackrf_set_freq(_device, tunerFreq());

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting frequency failed: ") + std::to_string(r));
	}

#ifdef HAVE_LIBHACKRF_ANTENNA_ENABLE
	r = hackrf_set_antenna_enable(_device, antennaPowerEnabled);

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting antenna power failed: ") + std::to_string(r));
	}
#else
	if (antennaPowerEnabled != 0) {
		std::cout << "Warning:\tantenna power option not available for installed libhackrf version" << std::endl;
	}
#endif // HAVE_LIBHACKRF_ANTENNA_ENABLE

	r = hackrf_set_amp_enable(_device, ampEnabled);

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting amplifier failed: ") + std::to_string(r));
	}

	r = hackrf_set_lna_gain(_device, lnaGain);

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting LNA gain failed: ") + std::to_string(r));
	}

	r = hackrf_set_vga_gain(_device, vgaGain);

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting VGA gain failed: ") + std::to_string(r));
	}

	r = hackrf_set_baseband_filter_bandwidth(_device, 1750000);

	if (r != HACKRF_SUCCESS) {
		hackrf_close(_device);
		hackrf_exit();
		throw std::runtime_error(std::string("HackRF setting baseband filter failed: ") + std::to_string(r));
	}
}

QGInputHackRF::~QGInputHackRF() {
	if (_device) {
		int r = hackrf_close(_device);

		if (r != HACKRF_SUCCESS) {
			std::cout << "HackRF closing failed: " << r << std::endl;
		}
	}

	hackrf_exit();
}

void QGInputHackRF::_startDevice() {
	int r = hackrf_start_rx(_device, async, this);

	if (r != HACKRF_SUCCESS) {
		throw std::runtime_error(std::string("HackRF run failed: ") + std::to_string(r));
	}

	return;
}

void QGInputHackRF::_stopDevice() {
	int r = hackrf_stop_rx(_device);

	if (r != HACKRF_SUCCESS) {
		throw std::runtime_error(std::string("HackRF stop failed: ") + std::to_string(r));
	}
}

void QGInputHackRF::_process(uint8_t *buf, int len) {
	// Drop new data if buffer full
	if (_bufferSize + len/2 > _bufferCapacity) {
		std::cout << "drop" << std::endl;
		return;
	}

	for (int j = 0; j < len;) {
  	// S8 IQ data
		signed char i = buf[j++];
		signed char q = buf[j++];
		_buffer[_bufferHead++] = std::complex<float>(i / 128., q / 128.);
		_bufferHead %= _bufferCapacity;
	}

	_bufferSize += len/2;
}

int QGInputHackRF::async(hackrf_transfer* transfer) {
	try {
		static_cast<QGInputHackRF *>(transfer->rx_ctx)->_process(transfer->buffer, transfer->valid_length);
	} catch (const std::exception &e) {
		// TODO return error,  reset device if error is from device read and ignore if from fft ?
	}

	return HACKRF_SUCCESS;
}
