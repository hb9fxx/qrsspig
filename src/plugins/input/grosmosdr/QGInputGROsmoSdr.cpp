#include "QGInputGROsmoSdr.h"

#include <iostream>
#include <stdexcept>

extern "C" QGInputDevice* create_object(const YAML::Node &config, int index) {
        return new QGInputGROsmoSdr(config, index);
}

extern "C" std::vector<std::string> list_devices() {
        return QGInputGROsmoSdr::listDevices();
}

std::vector<std::string> QGInputGROsmoSdr::listDevices() {
	std::vector<std::string> list;

	osmosdr::devices_t devices = osmosdr::device::find();

  std::transform(
    std::begin(devices),
    std::end(devices),
    std::back_inserter(list),
    [](auto const & e){ return e.to_string(); }
  );

	return list;
}

QGInputGROsmoSdr::QGInputGROsmoSdr(const YAML::Node &config, int index)
: QGInputDevice("grosmosdr", index, config) {
	_type = "GROsmoSdr";
}

QGInputGROsmoSdr::~QGInputGROsmoSdr() {
}

void QGInputGROsmoSdr::_startDevice() {
}

void QGInputGROsmoSdr::_stopDevice() {
}
