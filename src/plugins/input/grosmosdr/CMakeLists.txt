if (WITH_INPUT_GROSMOSDR)
	find_package(LibGROsmoSdr)

	if (LibGROsmoSdr_FOUND)

		set(Module "input_grosmosdr")

		add_library(${Module} SHARED
			QGInputGROsmoSdr.cpp
		)

		target_include_directories(${Module} PUBLIC ${LibGROsmoSdr_INCLUDE_DIRS})
		target_link_libraries(${Module} ${LibGROsmoSdr_LIBRARIES})

		install(TARGETS ${Module} LIBRARY DESTINATION lib/qrsspig)

		set(HAVE_LIBGROSMOSDR 1)
	endif (LibGROsmoSdr_FOUND)
else (WITH_INPUT_GROSMOSDR)
  message (STATUS "GROsmoSdr input disabled")
endif (WITH_INPUT_GROSMOSDR)
