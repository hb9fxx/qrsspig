#pragma once

#include "QGInputDevice.h"

#include <libairspy/airspy.h>

class QGInputAirSpy: public QGInputDevice {
public:
	static std::string moduleInfo();
	static std::vector<std::string> listDevices();

	QGInputAirSpy(const YAML::Node &config, int index);
	~QGInputAirSpy();

private:
	void _startDevice();
	void _stopDevice();

	struct airspy_device *_device;
};
