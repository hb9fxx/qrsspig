#pragma once

#include <chrono>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "QGInputDevice.h"
#include "QGProcessor.h"
#include "QGOutput.h"
#include "QGUploader.h"

class QrssPiG {
public:
	static void listModules();
	static void listDevices();

	explicit QrssPiG(const std::string &configFile);

	void run();
	void stop();

private:
	std::shared_ptr<QGInputDevice> _inputDevice;
	std::shared_ptr<QGProcessor> _processor;
	std::vector<std::shared_ptr<QGOutput>> _outputs;
	std::vector<std::shared_ptr<QGUploader>> _uploaders;

	std::chrono::milliseconds _lastCtrlC;
};
