#include "QGUploader.h"
#include "Config.h"

#include <cstring>
#include <functional>
#include <iostream>
#include <syslog.h>
#include <thread>

#include "QGFileData.h"
#include "QGOutput.h"
#include "QGPluginLoader.h"
#include "QGUtils.h"

std::vector<std::string> QGUploader::listModules() {
	QGPluginLoader<QGUploader> loader{"upload"};

	return loader.list_plugins();
}

std::unique_ptr<QGUploader> QGUploader::CreateUploader(
	int index,
	const YAML::Node &config
) {
	if (!config["type"]) {
		throw std::runtime_error("YAML: uploader must have a type");
	}

	auto type = config["type"].as<std::string>();

	QGPluginLoader<QGUploader> loader{"upload"};

	return std::unique_ptr<QGUploader>(loader.create(type, index, config));
}

QGUploader::QGUploader(
	std::string const& pluginId, int index, YAML::Node const &config
) :
QGPlugin(std::string("upload:") + pluginId, index, config),
_verbose{false},
_pushIntermediate{false},
_fileNameTmpl{} {
  if (config["filename"]) _fileNameTmpl = config["filename"].as<std::string>();
  if (config["verbose"]) _verbose = config["verbose"].as<bool>();
  if (config["intermediate"]) _pushIntermediate = config["intermediate"].as<bool>();
}

// Push is done in a thread on a copy of the data.
// Parent class handles copying of the data, creation of the thread and finally free the data
// wait param can be set to true to block until pushed. Used on program exit

void QGUploader::push(
	std::shared_ptr<QGFileData> const& data,
	std::string const& fileNameTmpl,
	bool intermediate,
	bool wait
) {
  if (intermediate && !_pushIntermediate) return;


	auto thread = std::thread(
		std::bind(&QGUploader::_pushThread, this, data, fileNameTmpl)
	);

  if (wait)
    thread.join();
  else
    thread.detach();
}

// Protected
// Override
void QGUploader::_checkSourceOk(std::shared_ptr<QGPlugin> const & source) {
	if (std::dynamic_pointer_cast<QGOutput>(source) == nullptr) {
		throw std::runtime_error("Uploader's source is not of output type");
	}
}

void QGUploader::_checkSinkOk(std::shared_ptr<QGPlugin> const & /*sink*/) {
	throw std::runtime_error("Uploaders don't support a sink plugin");
}

// Private
void QGUploader::_pushThread(
	std::shared_ptr<QGFileData> const & data,
	std::string const & fileNameTmpl
) {
  std::string fileName = QGUtils::formatFilename(
    _fileNameTmpl.length() > 0 ? _fileNameTmpl : fileNameTmpl,
    data->dispFreq,
    data->frameStart
  );
  fileName += "." + data->fileNameExt;

	std::string uri;
  try {
    _pushThreadImpl(fileName, data->getData(), data->getDataSize(), uri);

    syslog(LOG_ERR, "pushed %s", uri.c_str());
    std::cout << "pushed " << uri << std::endl;
  } catch (const std::exception &e) {
    syslog(LOG_ERR, "pushing %s failed: %s", uri.c_str(), e.what());
    std::cout << "pushing " << uri << " failed: " << e.what() << std::endl;
  }
}
