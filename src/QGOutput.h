#pragma once

#include "QGPlugin.h"

#include <chrono>
#include <complex>
#include <map>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include "QGFileData.h"

class QGUploader;

class QGOutput: public QGPlugin {
public:
	static std::vector<std::string> listModules();

	static std::unique_ptr<QGOutput> CreateOutput(
		int index,
		const YAML::Node &config
	);

	QGOutput(std::string const& pluginId, int index, YAML::Node const &config)
		: QGPlugin{std::string("output:") + pluginId, index, config} {};
	virtual ~QGOutput() {};

	virtual void addLine(const std::complex<float> * /*fft*/) {};

protected:
	// Override
	virtual void _checkSourceOk(std::shared_ptr<QGPlugin> const & source) override;
	virtual void _checkSinkOk(std::shared_ptr<QGPlugin> const & sink) override;

    void upload(std::shared_ptr<QGFileData> data, std::string const& fileNameTmpl, bool intermediate, bool wait);
    void upload(QGPluginConnection const & connection, std::shared_ptr<QGFileData> data, std::string const& fileNameTmpl, bool intermediate, bool wait);
};
