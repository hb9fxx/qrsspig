#pragma once

#include <chrono>
#include <string>


class QGFileData {
public:
	QGFileData(
    const std::string& fileNameExt,
    double dispFreq,
    std::chrono::milliseconds frameStart
  ) :
	fileNameExt(fileNameExt),
	dispFreq(dispFreq),
	frameStart(frameStart) {};
	virtual ~QGFileData() = default;

	virtual char const * getData() = 0;
	virtual unsigned int getDataSize() = 0;

  const std::string fileNameExt;
  const double dispFreq;
  const std::chrono::milliseconds frameStart;
};
