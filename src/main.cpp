#include <csignal>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#include <clara.hpp>

#include "Config.h"
#include "QrssPiG.h"

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

std::atomic<bool> gRestart{false};
std::unique_ptr<QrssPiG> gPig{nullptr};

void signalHandler(int signal) {
	if (signal == SIGHUP) {
		syslog(LOG_INFO, "SIGHUP received, restarting");
		std::cerr << "SIGHUP received, restarting" << std::endl;
		gRestart = true;
	}

	if (gPig) {
		gPig->stop();
	}
}

void daemonize() {
	pid_t pid;

	pid = fork();

	if (pid < 0) {
		throw std::runtime_error("Unable to detach: fork failed");
	}

	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	if (setsid() < 0) {
		throw std::runtime_error("Unable to detach: setsid failed");
	}

	pid = fork();

	if (pid < 0) {
		throw std::runtime_error("Unable to detach: seconde fork failed");
	}

	if (pid > 0) {
        exit(EXIT_SUCCESS);
	}

	umask(0);
//	chdir("/"); // Don't change dir as a local config file given as parameter wont be found

	int x;
	for (x = sysconf(_SC_OPEN_MAX); x >= 0; x--) {
		close(x);
	}

	openlog("qrsspig", LOG_PID, LOG_USER);
}

int main(int argc, char *argv[]) {
	std::cout << QRSSPIG_NAME << " v" << QRSSPIG_VERSION_MAJOR << "." << QRSSPIG_VERSION_MINOR << "." << QRSSPIG_VERSION_PATCH << std::endl;

	try {
		bool help{false};
		bool listmodules{false};
		bool listdevices{false};
		bool dummyoutput{false};
		bool unittests{false};
		bool detach{false};
		std::string configfile;

		using namespace clara;

		auto cli =
			Help(help)
			| Opt(listmodules)["-m"]["--listmodules"]("List modules")
			| Opt(listdevices)["-l"]["--listdevices"]("List devices")
			| Opt(dummyoutput)["-y"]["--dummyoutput"]("Generate dummy output")
			| Opt(unittests)["-t"]["--runtests"]("Run tests")
			| Opt(detach)["-d"]["--detach"]("Detach after start to run as a daemon")
			| Opt(configfile, "configfile")["-c"]["--configfile"]("Config file")
			| Arg(configfile, "configfile")("Config file");

		auto result = cli.parse(Args(argc, argv));

		if (!result) {
			std::cerr << std::endl << "Error in command line: " << result.errorMessage() << std::endl << std::endl;
			std::cout << cli << std::endl;
			exit(1);
		}

		bool stop = false;

		if (help) {
			std::cout << cli << std::endl;
			stop = true;
		}

		if (listmodules) {
			QrssPiG::listModules();
			stop = true;
		}

		if (listdevices) {
			QrssPiG::listDevices();
			stop = true;
		}

		if (dummyoutput) {
			if (configfile.empty()) {
                std::cerr << std::endl << "Error: --dummyoutput requires configfile" << std::endl << std::endl;
                std::cout << cli << std::endl;
                exit(1);
			} else {
                QrssPiG{configfile};
			}
			stop = true;
		}

		if (unittests) {
			// Disable all command line options.
			// To use catch options, we should first filter at least the --unittests arg
			// Return catch's return code
			return Catch::Session().run(1, argv);
		}

		if (stop) return 0;

		if (configfile.empty()) {
			std::cerr << std::endl << "Error: QrssPiG requires configfile" << std::endl << std::endl;
			std::cout << cli << std::endl;
			exit(1);
		}

		if (detach) {
			daemonize();
		}

		signal(SIGABRT, signalHandler);
		signal(SIGHUP, signalHandler);
		signal(SIGINT, signalHandler);
		signal(SIGTERM, signalHandler);

		do {
			gRestart = false;
			gPig = std::make_unique<QrssPiG>(configfile);
			gPig->run();
			gPig.reset();
		} while (gRestart);
	} catch (const std::exception &ex) {
		syslog(LOG_ERR, "%s", ex.what());
		std::cerr << "Error: " << ex.what() << std::endl;
		exit(-1);
	}

	return 0;
}
