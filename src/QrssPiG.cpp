#include "QrssPiG.h"

#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <syslog.h>

#include <yaml-cpp/yaml.h>

#include "QGPluginLoader.h"

void QrssPiG::listModules() {
	std::cout << "Plugin path:" << std::endl;
	std::cout << "\t" << QGPluginLoader<QGInputDevice>::plugin_path() << std::endl;
	std::cout << std::endl;

	std::cout << "Available input plugins:" << std::endl;
	for (const auto&m: QGInputDevice::listModules()) {
		std::cout << "\t" << std::left << std::setw(12) << std::string(m.first + ":") << m.second << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Available output plugins:" << std::endl;
	for (const auto&m: QGOutput::listModules()) {
		std::cout << "\t" << m << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Available upload plugins:" << std::endl;
	for (const auto&m: QGUploader::listModules()) {
		std::cout << "\t" << m << std::endl;
	}
	std::cout << std::endl;
}

void QrssPiG::listDevices() {
	auto devices = QGInputDevice::listDevices();

	std::cout << "Available input devices:" << std::endl;
	for (const auto& m: devices) {
		if (m.second.size()) {
			for (const auto &d: m.second) {
				std::cout << "\t" << std::left << std::setw(12) << std::string(m.first + ":") << d << std::endl;
			}
		} else {
			std::cout << "\t" << std::left << std::setw(12) << std::string(m.first + ":") << "No device found" << std::endl;
		}
	}
	std::cout << std::endl;
}

QrssPiG::QrssPiG(const std::string &configFile) : _lastCtrlC(0) {
	YAML::Node config = YAML::LoadFile(configFile);

	syslog(LOG_INFO, "Configuring");

	// Create input device
	if (config["input"] && !config["input"].IsMap()) {
		throw std::runtime_error("YAML: input must be a map");
	}
	_inputDevice = QGInputDevice::CreateInputDevice(0, config["input"]);

	// Create processor
	if (config["processing"] && !config["processing"].IsMap()) {
		throw std::runtime_error("YAML: processing must be a map");
	}
	_processor = std::make_shared<QGProcessor>("processor", 0, config["processing"]);

	// Connect input to processor
	QGPlugin::connect(_inputDevice, _processor);

	// Create all outputs
	if (config["output"]) {
		if (config["output"].IsMap()) {
			_outputs.push_back(QGOutput::CreateOutput(0, config["output"]));
		} else if (config["output"].IsSequence()) {
			unsigned int i = 0;

			std::transform(
				std::begin(config["output"]),
				std::end(config["output"]),
				std::back_inserter(_outputs),
				[&i](const auto & e) {
					return QGOutput::CreateOutput(i++, e);
				}
			);
		} else {
			throw std::runtime_error("YAML: output must be a map or a sequence");
		}
	}

    QGPlugin::connect(_processor, _outputs);

	// Create all uploader
	if (config["upload"]) {
		if (config["upload"].IsMap()) {
			_uploaders.push_back(QGUploader::CreateUploader(0, config["upload"]));
		} else if (config["upload"].IsSequence()) {
			unsigned int i = 0;

			std::transform(
				std::begin(config["upload"]),
				std::end(config["upload"]),
				std::back_inserter(_uploaders),
				[&i](const auto & e) { return QGUploader::CreateUploader(i++, e); }
			);
		} else {
			throw std::runtime_error("YAML: upload must be a map or a sequence");
		}
	}

	// Connect outputs to specified uploaders or all uploaders if none specified
	QGPlugin::connect(_outputs, _uploaders);

	std::cout << "Configured" << std::endl;
}

void QrssPiG::run() {
	syslog(LOG_INFO, "Started");
	std::cout << "Started" << std::endl;
	_inputDevice->run();
}

void QrssPiG::stop() {
	if (_lastCtrlC.count()) {
		auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());

		if ((now - _lastCtrlC).count() < 1000) {
			syslog(LOG_INFO, "Forced stop");
			std::cout << "Forced stop" << std::endl;
			exit(0);
		} else {
			syslog(LOG_INFO, "Already stopping... Press Ctrl-C a second time in less than 1 second for forced stop");
			std::cout << "Already stopping... Press Ctrl-C a second time in less than 1 second for forced stop" << std::endl;
		}

		_lastCtrlC = now;
	} else {
		_lastCtrlC = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
		syslog(LOG_INFO, "Stopping... (press Ctrl-C a second time in less than 1 second for)");
		std::cout << "Stopping" << std::endl;
		_inputDevice->stop();
		syslog(LOG_INFO, "Stopped");
		std::cout << "Stopped" << std::endl;
	}
}
