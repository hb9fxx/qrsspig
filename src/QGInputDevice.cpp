#include "QGInputDevice.h"
#include "QGPluginLoader.h"
#include "QGProcessor.h"

#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>
#include <thread>

#include "Config.h"

std::vector<std::pair<std::string, std::string>> QGInputDevice::listModules() {
	std::vector<std::pair<std::string, std::string>> modules;
	QGPluginLoader<QGInputDevice> loader{"input"};

	for (auto const& module: loader.list_plugins()) {
		std::string info;

		if (loader.has_module_info(module)) {
			info = loader.module_info(module);
		}

		modules.push_back(std::make_pair(module, info));
	}

	return modules;
}

std::vector<std::pair<std::string, std::vector<std::string>>> QGInputDevice::listDevices() {
	std::vector<std::pair<std::string, std::vector<std::string>>> devices;
	QGPluginLoader<QGInputDevice> loader{"input"};

	for (auto const& module: loader.list_plugins()) {
		if (loader.has_list_devices(module)) {
			try {
				std::vector<std::string> device_list = loader.list_devices(module);
				devices.push_back(std::make_pair(module, device_list));
			} catch (std::runtime_error const & e) {
				;
			}
		}
	}

	return devices;
}

std::unique_ptr<QGInputDevice> QGInputDevice::CreateInputDevice(
	int index,
	const YAML::Node &config
) {
	if (!config["type"]) {
		throw std::runtime_error("YAML: input must have a type");
	}

	auto type = config["type"].as<std::string>();

	QGPluginLoader<QGInputDevice> loader{"input"};

	return std::unique_ptr<QGInputDevice>(loader.create(type, index, config));
}

QGInputDevice::QGInputDevice(std::string const& pluginId, int index, YAML::Node const &config) :
QGPlugin{std::string("input:") + pluginId, index, config},
_noDevice{false},
_bufferCapacity{0},
_bufferSize{0},
_bufferHead{0},
_bufferTail{0},
_running{false},
_chunkSize{32} {
	_sampleRate = 48000;
  _dispFreq = 0.;
  _ppm = 0.;
  _offset = 0.;
  _offsetPpm = 0.;
	_bufferlength = 1000;
  _debugBufferMonitor = false;

  if (config["samplerate"]) _sampleRate = config["samplerate"].as<unsigned int>();
  if (config["basefreq"]) _dispFreq = config["basefreq"].as<double>();
  if (config["ppm"]) _ppm = config["ppm"].as<double>();
  if (config["offset"]) _offset = config["offset"].as<double>();
  if (config["offsetppm"]) _offsetPpm = config["offsetppm"].as<double>();
  if (config["bufferlength"]) _bufferlength = config["bufferlength"].as<int>();

  if (config["debug"]) {
      if (config["debug"]["buffermonitor"]) _debugBufferMonitor = config["debug"]["buffermonitor"].as<bool>();
  }

  // Do not open a device, for unit testing
  if (config["nodevice"]) _noDevice = config["nodevice"].as<bool>();

  _calcTunerFreq();
  _calcCenterFreq();
}

void QGInputDevice::run() {
	if (_running) throw std::runtime_error("Already running");

	// Ugly, use first exiting processor to get chunksize
	// For now it is ok, there is always one and only one processor
	for (auto const& connection: this->connections()) {
		auto plugin = connection.getPlugin();

		if (plugin) {
			auto processor = std::dynamic_pointer_cast<QGProcessor>(plugin);

			// _checkSinkOk already checked that all sinks are processors
			if (processor) {
				_chunkSize = processor->chunkSize();

				// buffercapacity based bufferlength in milliseconds,
				// rounded down to multiple of chunksize
				_bufferCapacity = _bufferlength / 1000. * _sampleRate;
				_bufferCapacity = (_bufferCapacity / _chunkSize) * _chunkSize;
				_buffer.resize(_bufferCapacity);

				break;
			}
		}
	}

	_running = true;
  bool stopping = false;
  bool running;

  _startDevice();

  std::thread monitor;
  if (_debugBufferMonitor) monitor = std::thread(std::bind(&QGInputDevice::_bufferMonitor, this));

  do {
    running = _running;

    // Detect stop() and shutdown device only once
    if (!running && !stopping) {
      _stopDevice();
      stopping = true;
    }

    if (_bufferSize >= _chunkSize) {
			for (auto const& connection: this->connections()) {
				auto plugin = connection.getPlugin();

				if (plugin) {
					std::dynamic_pointer_cast<QGProcessor>(plugin)->addIQ(begin(_buffer) + _bufferTail);
				}
			}
			_bufferTail += _chunkSize;
			_bufferTail %= _bufferCapacity;

			// Update size
			_bufferSize -= _chunkSize;
    } else {
			std::this_thread::sleep_for(std::chrono::microseconds(100));
    }
  } while (running || (_bufferSize >= _chunkSize));

  if (monitor.joinable()) monitor.join();
}

void QGInputDevice::stop() {
    // Called from signal handler, only do signal handler safe stuff here !
    _running = false;
}

// Protected
// Override
void QGInputDevice::_checkSourceOk(std::shared_ptr<QGPlugin> const & /*source*/) {
	throw std::runtime_error("Input devices don't support a source plugin");
}

void QGInputDevice::_checkSinkOk(std::shared_ptr<QGPlugin> const & sink) {
	if (std::dynamic_pointer_cast<QGProcessor>(sink) == nullptr) {
		throw std::runtime_error("Input device's sink is not of processor type");
	}
}

void QGInputDevice::_updatePPM(double ppm) {
  _ppm = ppm;
  _calcTunerFreq();
  _calcCenterFreq();
}

void QGInputDevice::_updateTunerFreq(double freq) {
	_tunerFreq = freq;
  _calcCenterFreq();
}

void QGInputDevice::_noTuner() {
  _tunerFreq = 0.;
  _centerFreq = _dispFreq + (_dispFreq * _ppm) / 1000000.;
}

// Private
void QGInputDevice::_calcTunerFreq() {
  double f = _offset + (_offset * _offsetPpm) / 1000000 + _dispFreq;
  _tunerFreq = f - (f * _ppm) / 1000000.;
  _centerFreq = _dispFreq;
}

void QGInputDevice::_calcCenterFreq() {
  double f = _offset + (_offset * _offsetPpm) / 1000000 + _dispFreq;
  double theoreticalTunerFreq = f - (f * _ppm) / 1000000.;
  _centerFreq = _dispFreq + (_tunerFreq - theoreticalTunerFreq);
}

void QGInputDevice::_bufferMonitor() {
    while (_running || _bufferSize >= _chunkSize) {
        std::this_thread::sleep_for(std::chrono::seconds(5));
        std::cout << (_running ? "[Running]" : "[Stopping]") << "\tBuffer: " << _bufferSize << " / " << _bufferCapacity << " (" << (100.*_bufferSize/_bufferCapacity) << "%)" << std::endl;
    }
    return;
}
