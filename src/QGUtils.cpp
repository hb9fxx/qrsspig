#include "QGUtils.h"
#include "Config.h"

#include <cmath>
#include <iomanip>
#include <stdexcept>
#include <string>

std::string QGUtils::formatFilename(const std::string &tmpl, long int freq, std::chrono::milliseconds frameStart) {
	time_t t = std::chrono::duration_cast<std::chrono::seconds>(frameStart).count();
	std::tm *tm = std::gmtime(&t);
	char s[32];

	std::string str = tmpl;
	size_t pos = 0;
	while (std::string::npos != (pos = str.find("%", pos))) {
		std::string sub;
		switch (str[pos + 1]) {
		case 'f':
			sub = std::to_string(freq);
			break;
		case 'D':
			std::strftime(s, sizeof(s), "%F", tm);
			sub = std::string(s);
			break;
		case 'd':
			std::strftime(s, sizeof(s), "%Y%m%d", tm);
			sub = std::string(s);
			break;
		case 'T':
			std::strftime(s, sizeof(s), "%T", tm);
			sub = std::string(s);
			break;
		case 't':
			std::strftime(s, sizeof(s), "%H%M%S", tm);
			sub = std::string(s);
			break;
		case 'I':
			std::strftime(s, sizeof(s), "%FT%TZ", tm);
			sub = std::string(s);
			break;
		case 'i':
			std::strftime(s, sizeof(s), "%Y%m%dT%H%M%SZ", tm);
			sub = std::string(s);
			break;
		case '%':
			sub = "%";
			break;
		default:
			sub = "";
		}

		str.replace(pos, 2, sub);
		pos += sub.length();
	}

	return str;
}

std::string QGUtils::levelBar(float v) {
	std::string c[] = {" ", "▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"};
	std::string s("");

	// Limit to [0, -100]dB interval
	if (v > 0.) v = 0.;
	if (v < -100.) v = -100.;

	double l; // Use double as modf not available on float on ubuntu 14.04/16.04
	long d = lround(trunc(modf((v + 100) / 2, &l) * 100 / 12.5));

	int i = 0;
	for (; i < l; i++) s += "█";
	if (d > 0) {
		s += c[d];
		i++;
	}
	for (; i < 50; i++) s += " ";

	return s;
}
