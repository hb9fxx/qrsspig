#include "catch.hpp"

#include <limits>
#include <memory>

#include "QGPluginLoader.h"
#include "QGUtils.h"


#include "tests/QGPluginTests.cpp"
#include "tests/QGInputDeviceTests.cpp"
#include "tests/QGProcessorTests.cpp"
#include "tests/QGFFTTests.cpp"
#include "tests/QGOutputTests.cpp"
#include "tests/QGUploaderTests.cpp"

TEST_CASE("QGPluginLoader", "[plugin]") {
  CHECK(QGPluginLoader<QGInputDevice>::plugin_path() == std::string(INSTALL_PREFIX) + "/lib/qrsspig/");
}

TEST_CASE("QGUtils formatFilename", "[utils]") {
  CHECK(QGUtils::formatFilename("", 14000000, std::chrono::milliseconds(0)) == "");
  CHECK(QGUtils::formatFilename("%f", 14000000, std::chrono::milliseconds(0)) == "14000000");
  CHECK(QGUtils::formatFilename("%D", 14000000, std::chrono::milliseconds(0)) == "1970-01-01");
  CHECK(QGUtils::formatFilename("%d", 14000000, std::chrono::milliseconds(0)) == "19700101");
  CHECK(QGUtils::formatFilename("%T", 14000000, std::chrono::milliseconds(0)) == "00:00:00");
  CHECK(QGUtils::formatFilename("%t", 14000000, std::chrono::milliseconds(0)) == "000000");
  CHECK(QGUtils::formatFilename("%I", 14000000, std::chrono::milliseconds(0)) == "1970-01-01T00:00:00Z");
  CHECK(QGUtils::formatFilename("%i", 14000000, std::chrono::milliseconds(0)) == "19700101T000000Z");
  CHECK(QGUtils::formatFilename("%%", 14000000, std::chrono::milliseconds(0)) == "%");
  CHECK(QGUtils::formatFilename("test_%fHz_%I", 14000000, std::chrono::milliseconds(0)) == "test_14000000Hz_1970-01-01T00:00:00Z");
  CHECK(QGUtils::formatFilename("test_%fHz_%i", 14000000, std::chrono::milliseconds(0)) == "test_14000000Hz_19700101T000000Z");
}

TEST_CASE("QGUtils levelBar", "[utils]") {
  CHECK(QGUtils::levelBar(std::numeric_limits<float>::infinity()) == "██████████████████████████████████████████████████");
  CHECK(QGUtils::levelBar(1000.)  == "██████████████████████████████████████████████████");
  CHECK(QGUtils::levelBar(0.)     == "██████████████████████████████████████████████████");
  CHECK(QGUtils::levelBar(-1.)    == "█████████████████████████████████████████████████▌");
  CHECK(QGUtils::levelBar(-2.)    == "█████████████████████████████████████████████████ ");
  CHECK(QGUtils::levelBar(-4.)    == "████████████████████████████████████████████████  ");
  CHECK(QGUtils::levelBar(-6.)    == "███████████████████████████████████████████████   ");
  CHECK(QGUtils::levelBar(-8.)    == "██████████████████████████████████████████████    ");
  CHECK(QGUtils::levelBar(-10.)   == "█████████████████████████████████████████████     ");
  CHECK(QGUtils::levelBar(-20.)   == "████████████████████████████████████████          ");
  CHECK(QGUtils::levelBar(-30.)   == "███████████████████████████████████               ");
  CHECK(QGUtils::levelBar(-40.)   == "██████████████████████████████                    ");
  CHECK(QGUtils::levelBar(-50.)   == "█████████████████████████                         ");
  CHECK(QGUtils::levelBar(-60.)   == "████████████████████                              ");
  CHECK(QGUtils::levelBar(-70.)   == "███████████████                                   ");
  CHECK(QGUtils::levelBar(-80.)   == "██████████                                        ");
  CHECK(QGUtils::levelBar(-90.)   == "█████                                             ");
  CHECK(QGUtils::levelBar(-100.)  == "                                                  ");
  CHECK(QGUtils::levelBar(-1000.) == "                                                  ");
  CHECK(QGUtils::levelBar(-std::numeric_limits<float>::infinity()) == "                                                  ");
}
