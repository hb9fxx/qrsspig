#pragma once

#include <algorithm>
#include <iterator>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

class QGPlugin;

class QGPluginConnection {
public:
	explicit QGPluginConnection(std::string const & name) :
	_name(name) {};
	QGPluginConnection(std::string const & name, std::map<std::string, std::string> const & config) :
	_name{name},
	_config{config} {};

	std::string const & getName() const { return _name; };
	std::map<std::string, std::string> const & getConfig() const { return _config; };
	void setPlugin(std::shared_ptr<QGPlugin> const & plugin) { _plugin = plugin; };
	std::shared_ptr<QGPlugin> const & getPlugin() const { return _plugin; };

private:
	std::string _name;
	std::map<std::string, std::string> _config;
	std::shared_ptr<QGPlugin> _plugin;
};

class QGPlugin {
public:
	QGPlugin(std::string const & pluginId, int index, YAML::Node const & config)
	: _config{config}, _has_connections{false}, _pluginId{pluginId}, _index{index} {
		if (config["name"]) {
			_name = config["name"].as<std::string>();
		}

		if (config["connections"]) {
			_has_connections = true;

			if (config["connections"].IsSequence()) {
				for (auto &connection: config["connections"]) {
					_addConnection(connection);
				}
			} else {
					_addConnection(config["connections"]);
			}
		}
		std::cout << "Created " << this->idString() << std::endl;
	};
	QGPlugin(QGPlugin const&) = delete;
	void operator=(QGPlugin const&) = delete;
	virtual ~QGPlugin() = default;

	// Return pluginId given at constructor
	std::string const& pluginId() const { return _pluginId; };

	// Return index givent at constructor
	int index() const { return _index; };

	// Return name found in config. Empty if none
	std::string const& name() const { return _name; };

	// Return id string, consisting of pluginId, index and name if any
	// in the form pluginId[index]:name
	std::string idString() const {
		using namespace std::literals::string_literals;
		return _pluginId + "[" + std::to_string(_index) + "]" + (!_name.empty() ? ":"s + _name : "");
	};

	// Static functions to connect to plugins
    template <typename T1, typename T2>
    static void connect(
            std::vector<std::shared_ptr<T1>> const & sources,
            std::vector<std::shared_ptr<T2>> const & sinks
    ) {
        std::for_each(std::begin(sources), std::end(sources), [&sinks](auto const & source) { connect(source, sinks); });
    }

    template <typename T>
    static void connect(
            std::shared_ptr<QGPlugin> const & source,
            std::vector<std::shared_ptr<T>> const & sinks
    ) {
        // Connect source to specified sinks or all sinks if no connection specified
        auto connections = source->connections();

        if (source->hasConnections()) {
            // Create sink map mapping name and sink index to index in vector
            std::map<std::string, int> sinkMap;
            for (auto const & sink: sinks) {
                sinkMap.insert(std::make_pair(std::to_string(sink->index()), sink->index()));
                if (sink->name().size()) {
                    sinkMap.insert(std::make_pair(sink->name(), sink->index()));
                }
            }

            for (auto const & connection: connections) {
                if (sinkMap.count(connection.getName())) {
                    connect(source, sinks[sinkMap.at(connection.getName())]);
                } else {
                    throw std::runtime_error(std::string("YAML: unknown uploader \"") + connection.getName() + "\" specified in output");
                }
            }
        } else {
            for (auto const & sink: sinks) {
                connect(source, sink);
            }
        }
    }

	static void connect(
		std::shared_ptr<QGPlugin> const & source,
		std::shared_ptr<QGPlugin> const & sink
	) {
		try {
			sink->_checkSourceOk(source);
			source->_checkSinkOk(sink);

			source->_addPluginToConnection(sink);
			sink->_addSource(source);
			source->_configureNewSink(sink);
			sink->_configureNewSource(source);

			std::cout << "Connected " << source->idString() << " to " << sink->idString() << std::endl;
		} catch (std::exception const & e) {
			using namespace std::literals::string_literals;
			throw std::runtime_error("Invalide connection "s + source->idString() + " to "s + sink->idString() + ": "s + e.what());
		}
	};

  // Get list of sources
	std::vector<std::string> sources() {
		std::vector<std::string> sources;

		// cbegin/cend not supported on jessie
		std::transform(
			std::begin(_sources),
			std::end(_sources),
			std::back_inserter(sources),
			[](auto const& source) {
				return source.lock()->idString();
			}
		);

		return sources;
	};

	// Get specific source instance
	std::shared_ptr<QGPlugin> sourcePlugin(std::string const & id) const {
		auto source = std::find_if(
			std::begin(_sources),
			std::end(_sources),
			[id](auto const & s){ return s.lock()->idString() == id; }
		);

		using namespace std::literals::string_literals;
		if (source == std::end(_sources)) {
			throw std::runtime_error("Source plugin with id "s + id + " unknown");
		}

		return source->lock();
	};

	bool hasConnections() { return _has_connections; }

	// Get list of connections
	std::vector<QGPluginConnection> const & connections() const {
		return _connections;
	};

protected:
	virtual void _checkSourceOk(std::shared_ptr<QGPlugin> const & /*source*/) {};
	virtual void _checkSinkOk(std::shared_ptr<QGPlugin> const & /*sink*/) {};
	virtual void _configureNewSource(std::shared_ptr<QGPlugin> const & /*source*/) {};
	virtual void _configureNewSink(std::shared_ptr<QGPlugin> const & /*sink*/) {};

protected:
		const YAML::Node _config;
		bool _has_connections;

private:
	// Register a source instance
	void _addSource(std::shared_ptr<QGPlugin> const & plugin) {
		_sources.insert(plugin);
	};

	// Parse connection's  yaml and register connection info
	void _addConnection(YAML::Node const& connection) {
		if (connection.IsScalar()) {
			// No param in scalar case, so register with empty params
			_connections.emplace_back(connection.as<std::string>());
		} else if (connection.IsMap()) {
			if (!connection["name"]) {
				throw std::runtime_error("YAML: connection is missing a name");
			}

			std::map<std::string, std::string> config;

			// cbegin/cend not supported on jessie
			std::transform(
				std::begin(connection),
				std::end(connection),
				std::inserter(config, std::end(config)),
				[](auto const& pair) {
					if (!pair.second.IsScalar()) {
						throw std::runtime_error("YAML: connection config value must be a scalar");
					}
					return std::make_pair(
						pair.first.template as<std::string>(),
						pair.second.template as<std::string>()
					);
				}
			);

			// Erase it only here after an uncessary copy/transform
			// Remove it in the yaml node after assigning to name would
			// discard const
			config.erase("name");

			_connections.emplace_back(connection["name"].as<std::string>(), config);
		} else {
			throw std::runtime_error("YAML: connection must be a scalar or a map");
		}
	}

    void _addPluginToConnection(std::shared_ptr<QGPlugin> const& plugin) {
        bool found{false};

        for (auto & connection: _connections) {
            // Compare by name
            if (connection.getName() == plugin->name()) {
                connection.setPlugin(plugin);
                found = true;
            } else {
                // Try to convert to int to compare by index
                try {
                    if (std::stoi(connection.getName()) == plugin->index()) {
                        connection.setPlugin(plugin);
                        found = true;
                    }
                }
                // std::invalid_argument or std::out_of_range could be thrown. Ignore and continue matching
                catch (...) {}
            }
        }

        // If nothing found, create a connection using idString as name
        if (!found) {
            QGPluginConnection connection{plugin->idString()};
            connection.setPlugin(plugin);
            _connections.push_back(connection);
        }
    };

	const std::string _pluginId;
	const int _index;
	std::string _name;

	std::set<std::weak_ptr<QGPlugin>, std::owner_less<std::weak_ptr<QGPlugin>>> _sources;
	std::vector<QGPluginConnection> _connections;
};
