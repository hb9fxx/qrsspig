#include "QGOutput.h"

#include <algorithm>
#include <iostream>
#include <stdexcept>

#include "QGPluginLoader.h"
#include "QGProcessor.h"
#include "QGUploader.h"

std::vector<std::string> QGOutput::listModules() {
	QGPluginLoader<QGOutput> loader{"output"};

	return loader.list_plugins();
}

std::unique_ptr<QGOutput> QGOutput::CreateOutput(
	int index,
	const YAML::Node &config
) {
	std::string type{"image"}; // default

	if (config["type"]) {
		type = config["type"].as<std::string>();
	}

	QGPluginLoader<QGOutput> loader{"output"};

	return std::unique_ptr<QGOutput>(loader.create(type, index, config));
}

// Protected
// Override
void QGOutput::_checkSourceOk(std::shared_ptr<QGPlugin> const & source) {
	if (std::dynamic_pointer_cast<QGProcessor>(source) == nullptr) {
		throw std::runtime_error("Output's source is not of processor type");
	}
}

void QGOutput::_checkSinkOk(std::shared_ptr<QGPlugin> const & sink) {
	if (std::dynamic_pointer_cast<QGUploader>(sink) == nullptr) {
		throw std::runtime_error("Output's source is not of upload type");
	}
}

void QGOutput::upload(
    std::shared_ptr<QGFileData> data,
    std::string const& fileNameTmpl,
    bool intermediate,
    bool wait
) {
    for (auto const& connection: this->connections()) {
        upload(connection, data, fileNameTmpl, intermediate, wait);
    }
}

void QGOutput::upload(
        QGPluginConnection const & connection,
        std::shared_ptr<QGFileData> data,
        std::string const& fileNameTmpl,
        bool intermediate,
        bool wait
) {
    auto plugin = connection.getPlugin();

    if (plugin) {
        auto config = connection.getConfig();

        // Override template with link specific template
        // TODO will need to support other file accepting plugins,
        // like transformers
        if (config.count("filename") == 1) {
            std::dynamic_pointer_cast<QGUploader>(plugin)->push(
                    data, config.at("filename"), intermediate, wait
            );
        } else {
            std::dynamic_pointer_cast<QGUploader>(plugin)->push(
                    data, fileNameTmpl, intermediate, wait
            );
        }
    }
}
