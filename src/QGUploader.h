#pragma once

#include "QGPlugin.h"

#include <chrono>
#include <memory>
#include <string>

#include <yaml-cpp/yaml.h>

class QGFileData;

class QGUploader: public QGPlugin {
public:
	static std::vector<std::string> listModules();

	static std::unique_ptr<QGUploader> CreateUploader(
		int index,
		const YAML::Node &config
	);

	QGUploader(std::string const& pluginId, int index, YAML::Node const &config);
	virtual ~QGUploader() {};

	void push(
		std::shared_ptr<QGFileData> const & data,
		std::string const & fileNameTmpl,
		bool intermediate = false,
		bool wait = false
	);

protected:
	// Override
	virtual void _checkSourceOk(std::shared_ptr<QGPlugin> const & source) override;
	virtual void _checkSinkOk(std::shared_ptr<QGPlugin> const & sink) override;

private:
	void _pushThread(
		std::shared_ptr<QGFileData> const & data,
		std::string const & fileNameTmpl
	);

	virtual void _pushThreadImpl(
		const std::string & /*fileName*/,
		const char * /*data*/,
		int /*dataSize*/,
		std::string & /*uri*/
	) {};

protected:
	bool _verbose;
	bool _pushIntermediate;
	std::string _fileNameTmpl;
};
