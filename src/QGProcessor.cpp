#include "QGProcessor.h"

#include "QGInputDevice.h"
#include "QGOutput.h"

#include <algorithm>
#include <cstring>
#include <iostream>
#include <math.h>
#include <stdexcept>

QGProcessor::QGProcessor(std::string const& pluginId, int index, YAML::Node const &config) :
QGPlugin{std::string("processor:") + pluginId, index, config},
_inputSampleRate{0},
_sampleRate{0},
_chunkSize{32},
_N{2048},
_overlap{ (3 * _N) / 4}, // As it depends on N, default initialization will be redone later even when not given in config
_rate{1.},
_noProcessor(false),
_inputIndex{0},
_fft{} {
	if (config["samplerate"]) _sampleRate = config["samplerate"].as<int>();
	if (config["chunksize"]) _chunkSize = config["chunksize"].as<int>();
	if (config["fft"]) _N = config["fft"].as<unsigned int>();
	if (config["fftoverlap"]) {
		unsigned int o = config["fftoverlap"].as<unsigned int>();
		if (o >= _N) throw std::runtime_error("YAML: overlap value out of range [0..N[");
		_overlap = (o * _N) / (o + 1);
	} else {
		_overlap = (3 * _N) / 4;
	}

	// Do not create resampler/fft, for faster unit testing where no processing is done
	if (config["noprocessor"]) _noProcessor = config["noprocessor"].as<bool>();
}

QGProcessor::~QGProcessor() {
	if (_rate != 1.0) {
		if (_liquidSdrResampler) resamp_crcf_destroy(_liquidSdrResampler);
	}
}

void QGProcessor::addIQ(std::vector<std::complex<float>>::const_iterator iq) {
	if (_rate != 1.0) {
		_inputIndex += _resample(iq, std::begin(_input) + _inputIndex);
	} else {
		std::copy(iq, iq + _chunkSize, &_input.front() + _inputIndex);
		_inputIndex += _chunkSize;
	}

	if (_inputIndex >= _N) {
		_computeFFT();

		// Shift overlap to buffer start
		std::copy(begin(_input) + _N - _overlap, begin(_input) + _inputIndex, begin(_input));
		_inputIndex = _overlap + _inputIndex - _N;
	}
}

// Protected
// Override
void QGProcessor::_checkSourceOk(std::shared_ptr<QGPlugin> const & source) {
	if (std::dynamic_pointer_cast<QGInputDevice>(source) == nullptr) {
		throw std::runtime_error("Processor's source is not of input device type");
	}
}

void QGProcessor::_checkSinkOk(std::shared_ptr<QGPlugin> const & sink) {
	if (std::dynamic_pointer_cast<QGOutput>(sink) == nullptr) {
		throw std::runtime_error("Processor's sink is not of output type");
	}
}

void QGProcessor::_configureNewSource(std::shared_ptr<QGPlugin> const & source) {
  auto input = std::dynamic_pointer_cast<QGInputDevice>(source);

	_inputSampleRate = input->sampleRate();

	if (_sampleRate && (_inputSampleRate != _sampleRate)) {
		_rate = (float)_inputSampleRate / (float)_sampleRate;

		// Correct with real samplerate from resampler
		_sampleRate = _inputSampleRate / _rate;
	}

	if (_sampleRate == 0) {
		_sampleRate = _inputSampleRate;
	}

	// Buffer
	_inputIndex = 0;
	// Add provision to add a full chunksize when only one sample left before reaching N. If resampling, will be smaller in worst case
	_input.resize(_N + _chunkSize - 1);

	// Window function
	_hannW.resize(_N);
	{
		unsigned int i = 0;
		std::generate_n(begin(_hannW), _N, [&i, this]() { return .5 * (1 - cos(2 * M_PI * i++ / _N)); });
	}

	// Initialize to null
  _liquidSdrResampler = nullptr;

	if (_noProcessor) return;

	// Resampler
	if (_rate != 1.0) {
		_liquidSdrResampler = resamp_crcf_create(1./_rate, 6000, .49/_rate, 60., 32);
	}

	// FFT
	_fft = std::make_unique<QGFFT>(_N);
}

// Private
unsigned int QGProcessor::_resample(std::vector<std::complex<float>>::const_iterator in, std::vector<std::complex<float>>::iterator out) {
	unsigned int outSize = 0;

	resamp_crcf_execute_block(_liquidSdrResampler, const_cast<std::complex<float>*>(&(*in)), _chunkSize, &(*out), &outSize);

	return outSize;
}

void QGProcessor::_computeFFT() {
	for (unsigned int i = 0; i < _N; i++) _fft->in()[i] = _input[i] * _hannW[i];
	_fft->execute();

	for (auto const& connection: this->connections()) {
		auto plugin = connection.getPlugin();

		if (plugin) {
			std::dynamic_pointer_cast<QGOutput>(plugin)->addLine(_fft->out());
		}
	}
}
