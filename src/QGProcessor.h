#pragma once

#include "Config.h"

#include "QGPlugin.h"

#include <complex> // Must be included before fftw3
#include <memory>
#include <vector>

#include <fftw3.h>
#include <liquid/liquid.h>
#include <yaml-cpp/yaml.h>

#include "QGFFT.h"

class QGProcessor: public QGPlugin {
public:
	QGProcessor(std::string const& pluginId, int index, YAML::Node const &config);
	virtual ~QGProcessor();

	void addIQ(std::vector<std::complex<float>>::const_iterator iq);

	unsigned int sampleRate() const { return _sampleRate; };
	unsigned int chunkSize() const { return _chunkSize; };
	unsigned int fftSize() const { return _N; };
	unsigned int fftOverlap() const { return _overlap; };
	float downsamplingRate() const { return _rate; };

protected:
	// Override
	virtual void _checkSourceOk(std::shared_ptr<QGPlugin> const & source) override;
	virtual void _checkSinkOk(std::shared_ptr<QGPlugin> const & sink) override;
	virtual void _configureNewSource(std::shared_ptr<QGPlugin> const & source) override;

private:
	unsigned int _resample(std::vector<std::complex<float>>::const_iterator in, std::vector<std::complex<float>>::iterator out);
	void _computeFFT();

	unsigned int _inputSampleRate;
	unsigned int _sampleRate;
	unsigned int _chunkSize;
	unsigned int _N;
	unsigned int _overlap; // 0: no overlap, 1: 1/2, 2: 2/3...
	float _rate;

	bool _noProcessor;

	// Input buffer
	unsigned int _inputIndex;
	std::vector<std::complex<float>> _input;

	// Window function
	std::vector<float> _hannW;

	// Resampler
	resamp_crcf _liquidSdrResampler;

	// FFT
	std::unique_ptr<QGFFT> _fft;
};
