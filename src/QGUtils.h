#pragma once

#include <chrono>
#include <string>

class QGUtils {
public:
	static std::string formatFilename(const std::string &tmpl, long int freq, std::chrono::milliseconds frameStart);
	static std::string levelBar(float v);
};
