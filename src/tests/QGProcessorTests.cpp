#include "QGProcessor.h"

TEST_CASE("QGProcessor", "[processor]") {
  QGPluginLoader<QGInputDevice>* plugin = new QGPluginLoader<QGInputDevice>("input");

  std::unique_ptr<QGInputDevice> device;
  REQUIRE_NOTHROW(
    device = QGInputDevice::CreateInputDevice(
      0,
      YAML::Load(
        "{type: stdin, nodevice: true, samplerate: 16000, basefreq: 7000000}"
      )
    )
  );

  // Check defaults
  std::unique_ptr<QGProcessor> processor;
  CHECK_NOTHROW(
    processor.reset(
      new QGProcessor(
        "processor",
        0,
        YAML::Load("{noprocessor: true}")
      )
    )
  );

  CHECK(processor->sampleRate() == 0); // No processor samplerate given, input not connected, so 0
  CHECK(processor->chunkSize() == 32);
  CHECK(processor->fftSize() == 2048);
  CHECK(processor->fftOverlap() == 1536);
  CHECK(processor->downsamplingRate() == 1.);

  delete plugin;
}

TEST_CASE("QGProcessorConnection", "[processor,connection]") {
  auto plugin = std::shared_ptr<QGPlugin>(new QGPlugin("plugin", 0, YAML::Load("{}")));
  auto device = std::shared_ptr<QGInputDevice>(new QGInputDevice("input", 0, YAML::Load("{}")));
  auto processor = std::shared_ptr<QGProcessor>(new QGProcessor("processor", 0, YAML::Load("{}")));
  auto output = std::shared_ptr<QGOutput>(new QGOutput("output", 0, YAML::Load("{}")));
  auto uploader = std::shared_ptr<QGUploader>(new QGUploader("uploader", 0, YAML::Load("{}")));

  CHECK_THROWS(QGPlugin::connect(plugin, processor));
  CHECK_NOTHROW(QGPlugin::connect(device, processor));
  CHECK_THROWS(QGPlugin::connect(processor, processor));
  CHECK_THROWS(QGPlugin::connect(output, processor));
  CHECK_THROWS(QGPlugin::connect(uploader, processor));

  CHECK_THROWS(QGPlugin::connect(processor, plugin));
  CHECK_THROWS(QGPlugin::connect(processor, device));
  CHECK_THROWS(QGPlugin::connect(processor, processor));
  CHECK_NOTHROW(QGPlugin::connect(processor, output));
  CHECK_THROWS(QGPlugin::connect(processor, uploader));
}
