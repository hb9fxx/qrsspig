#include <QGPlugin.h>

#include <memory>

TEST_CASE("QGPlugin no name", "[plugin]") {
  QGPlugin plugin("testplugin", 0, YAML::Load("{}"));
  CHECK(plugin.pluginId() == "testplugin");
  CHECK(plugin.index() == 0);
  CHECK(plugin.name() == "");
  CHECK(plugin.idString() == "testplugin[0]");
  CHECK(plugin.connections().size() == 0);
}

TEST_CASE("QGPlugin with name", "[plugin]") {
  QGPlugin plugin("othertestplugin", 1, YAML::Load("{name: myothertestplugin}"));
  CHECK(plugin.pluginId() == "othertestplugin");
  CHECK(plugin.index() == 1);
  CHECK(plugin.name() == "myothertestplugin");
  CHECK(plugin.idString() == "othertestplugin[1]:myothertestplugin");
  CHECK(plugin.connections().size() == 0);
}

TEST_CASE("QGPlugin with scalar connection", "[plugin]") {
  QGPlugin plugin("testplugin", 0, YAML::Load("{name: plugin, connections: con}"));
  CHECK(plugin.pluginId() == "testplugin");
  CHECK(plugin.index() == 0);
  CHECK(plugin.name() == "plugin");
  CHECK(plugin.idString() == "testplugin[0]:plugin");

  auto connections = plugin.connections();
  CHECK(connections.size() == 1);

  CHECK(connections.at(0).getName() == "con");
  CHECK(connections.at(0).getConfig().size() == 0);
  CHECK(connections.at(0).getPlugin() == nullptr);
}

TEST_CASE("QGPlugin with map connection", "[plugin]") {
  QGPlugin plugin("testplugin", 0, YAML::Load("{name: plugin, connections: {name: con, param: value}}"));
  CHECK(plugin.pluginId() == "testplugin");
  CHECK(plugin.index() == 0);
  CHECK(plugin.name() == "plugin");
  CHECK(plugin.idString() == "testplugin[0]:plugin");

  auto connections = plugin.connections();
  CHECK(connections.size() == 1);

  CHECK(connections.at(0).getName() == "con");

  auto config = connections.at(0).getConfig();
  CHECK(config.size() == 1);
  CHECK(config.count("param") == 1);
  CHECK(config["param"] == "value");

  CHECK(connections.at(0).getPlugin() == nullptr);
}

TEST_CASE("QGPlugin with mixed list of connections including two connections to same sink", "[plugin]") {
    QGPlugin plugin("testplugin", 1, YAML::Load("{name: plugin, connections: [con1, {name: con2, param: value1}, {name: con1, param: value2}]}"));
    CHECK(plugin.pluginId() == "testplugin");
    CHECK(plugin.index() == 1);
    CHECK(plugin.name() == "plugin");
    CHECK(plugin.idString() == "testplugin[1]:plugin");

    auto connections = plugin.connections();
    CHECK(connections.size() == 3);

    CHECK(connections.at(0).getName() == "con1");

    auto config = connections.at(0).getConfig();
    CHECK(config.size() == 0);

    CHECK(connections.at(0).getPlugin() == nullptr);

    CHECK(connections.at(1).getName() == "con2");

    config = connections.at(1).getConfig();
    CHECK(config.size() == 1);
    CHECK(config.count("param") == 1);
    CHECK(config["param"] == "value1");

    CHECK(connections.at(1).getPlugin() == nullptr);

    CHECK(connections.at(2).getName() == "con1");

    config = connections.at(2).getConfig();
    CHECK(config.size() == 1);
    CHECK(config.count("param") == 1);
    CHECK(config["param"] == "value2");

    CHECK(connections.at(0).getPlugin() == nullptr);

    CHECK_THROWS(config = connections.at(3).getConfig());
}

TEST_CASE("QGPlugin connection matrix including two connection to same sink and connection by id", "[plugin]") {
    auto source_1 = std::make_shared<QGPlugin>("testsource", 0, YAML::Load("{name: source_1, connections: [sink_1, {name: sink_2, type: value1}, {name: sink_1, type: value2}, 0]}"));
    auto source_2 = std::make_shared<QGPlugin>("testsource", 1, YAML::Load("{name: source_2, connections: [{name: sink_2, type: value3}, {name: 2, type: value4}]}"));
    auto source_3 = std::make_shared<QGPlugin>("testsource", 2, YAML::Load("{name: source_3}"));

    auto sink_1 = std::make_shared<QGPlugin>("testsink", 0, YAML::Load("{name: sink_1}"));
    auto sink_2 = std::make_shared<QGPlugin>("testsink", 1, YAML::Load("{name: sink_2}"));
    auto sink_3 = std::make_shared<QGPlugin>("testsink", 2, YAML::Load("{name: sink_3}"));

    std::vector<std::shared_ptr<QGPlugin>> source_list{source_1, source_2, source_3};
    std::vector<std::shared_ptr<QGPlugin>> sink_list{sink_1, sink_2, sink_3};

    QGPlugin::connect(source_list, sink_list);

    // Test source connections
    // Source 1
    auto connections = source_1->connections();
    CHECK(connections.size() == 4);

    CHECK(connections.at(0).getName() == "sink_1");
    auto returned_plugin = connections.at(0).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[0]:sink_1");

    CHECK(connections.at(1).getName() == "sink_2");
    returned_plugin = connections.at(1).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[1]:sink_2");

    CHECK(connections.at(2).getName() == "sink_1");
    returned_plugin = connections.at(2).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[0]:sink_1");

    CHECK(connections.at(3).getName() == "0");
    returned_plugin = connections.at(3).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[0]:sink_1");

    // Source 2
    connections = source_2->connections();
    CHECK(connections.size() == 2);

    CHECK(connections.at(0).getName() == "sink_2");
    returned_plugin = connections.at(0).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[1]:sink_2");

    CHECK(connections.at(1).getName() == "2");
    returned_plugin = connections.at(1).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[2]:sink_3");

    // Source 3
    // As no connections specified, it should be connected to all
    // using the idString as name
    connections = source_3->connections();
    CHECK(connections.size() == 3);

    CHECK(connections.at(0).getName() == "testsink[0]:sink_1");
    returned_plugin = connections.at(0).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[0]:sink_1");

    CHECK(connections.at(1).getName() == "testsink[1]:sink_2");
    returned_plugin = connections.at(1).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[1]:sink_2");

    CHECK(connections.at(2).getName() == "testsink[2]:sink_3");
    returned_plugin = connections.at(2).getPlugin();
    REQUIRE(returned_plugin != nullptr);
    CHECK(returned_plugin->idString() == "testsink[2]:sink_3");

    // Test sink sources
    std::vector<std::string> sources;
    std::shared_ptr<QGPlugin> source;

    // Sink 1
    REQUIRE_NOTHROW(sources = sink_1->sources());
    CHECK(sources.size() == 2);

    REQUIRE(std::count(
        std::begin(sources),
        std::end(sources),
        "testsource[0]:source_1"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_1->sourcePlugin("testsource[0]:source_1"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[0]:source_1");

    REQUIRE(std::count(
            std::begin(sources),
            std::end(sources),
            "testsource[2]:source_3"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_1->sourcePlugin("testsource[2]:source_3"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[2]:source_3");

    // Sink 2
    REQUIRE_NOTHROW(sources = sink_2->sources());
    CHECK(sources.size() == 3);

    REQUIRE(std::count(
            std::begin(sources),
            std::end(sources),
            "testsource[0]:source_1"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_2->sourcePlugin("testsource[0]:source_1"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[0]:source_1");

    REQUIRE(std::count(
            std::begin(sources),
            std::end(sources),
            "testsource[1]:source_2"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_2->sourcePlugin("testsource[1]:source_2"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[1]:source_2");

    REQUIRE(std::count(
            std::begin(sources),
            std::end(sources),
            "testsource[2]:source_3"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_2->sourcePlugin("testsource[2]:source_3"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[2]:source_3");

    // Sink 3
    REQUIRE_NOTHROW(sources = sink_3->sources());
    CHECK(sources.size() == 2);

    REQUIRE(std::count(
            std::begin(sources),
            std::end(sources),
            "testsource[1]:source_2"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_3->sourcePlugin("testsource[1]:source_2"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[1]:source_2");

    REQUIRE(std::count(
            std::begin(sources),
            std::end(sources),
            "testsource[2]:source_3"
    ) == 1);
    REQUIRE_NOTHROW(source = sink_3->sourcePlugin("testsource[2]:source_3"));
    CHECK(source != nullptr);
    CHECK(source->idString() == "testsource[2]:source_3");
}

TEST_CASE("QGPlugin with invalid connections", "[plugin]") {
  CHECK_THROWS(QGPlugin("testplugin", 0, YAML::Load("{name: plugin, connections: {name: con2, type: [one, two]}}")));
}
