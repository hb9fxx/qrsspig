#include <QGFFT.h>

TEST_CASE("QGFFT", "[fft]") {
    int n{8};
    QGFFT fft{n};

    CHECK(fft.n() == n);
    CHECK(fft.in() != nullptr);
    CHECK(fft.out() != nullptr);

    for (int i = 0; i < n; i++) {
        fft.in()[i] = std::complex<float>(0, 0);
        fft.out()[i] = std::complex<float>(-1, -1);
    }

    CHECK_NOTHROW(fft.execute());

    for (int i = 0; i < n; i++) {
        CHECK(fft.out()[i] == std::complex<float>(0, 0));
    }
}

TEST_CASE("QGFFT_freq2Bucket", "[fft]") {
    int n{8};
    QGFFT fft{n};
    std::vector<int> ref{5, 6, 7, 0, 1, 2, 3, 4};

    int i = 0;
    for (double f = -n/2 + 1; f < n/2 + 1; i++, f++) {
        CHECK(fft.freq2Bucket(f, n) == ref[i]);
    }
    CHECK_THROWS(fft.freq2Bucket(-n/2, n));
    CHECK_THROWS(fft.freq2Bucket(n/2 + 1, n));

    CHECK(fft.freq2Bucket(0, n) == 0);
    CHECK(fft.freq2Bucket(2, n) == 2);
    CHECK(fft.freq2Bucket(-3, n) == 5);
}

TEST_CASE("QGFFT_bucket2Freq", "[fft]") {
    int n{8};
    QGFFT fft{n};
    std::vector<int> ref{0, 1, 2, 3, 4, -3, -2, -1};

    for (int i = 0; i < n; i++) {
        CHECK(fft.bucket2Freq(i, n) == ref[i]);
    }
    CHECK_THROWS(fft.bucket2Freq(-1, n));
    CHECK_THROWS(fft.bucket2Freq(n, n));
}

TEST_CASE("QGFFT_n4res", "[fft]") {
    CHECK(QGFFT::n4res(0.42, 16) == 64);
    CHECK(QGFFT::n4res(0.8, 16) == 32);
    CHECK(QGFFT::n4res(1, 16) == 16);
    CHECK(QGFFT::n4res(1.2, 16) == 16);
    CHECK(QGFFT::n4res(2.5, 16) == 8);
}
