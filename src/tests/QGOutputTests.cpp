#include <QGOutput.h>

TEST_CASE("QGOutputConnection", "[output,connection]") {
  auto plugin = std::shared_ptr<QGPlugin>(new QGPlugin("plugin", 0, YAML::Load("{}")));
  auto device = std::shared_ptr<QGInputDevice>(new QGInputDevice("input", 0, YAML::Load("{}")));
  auto processor = std::shared_ptr<QGProcessor>(new QGProcessor("processor", 0, YAML::Load("{}")));
  auto output = std::shared_ptr<QGOutput>(new QGOutput("output", 0, YAML::Load("{}")));
  auto uploader = std::shared_ptr<QGUploader>(new QGUploader("uploader", 0, YAML::Load("{}")));

  CHECK_THROWS(QGPlugin::connect(plugin, output));
  CHECK_THROWS(QGPlugin::connect(device, output));
  CHECK_NOTHROW(QGPlugin::connect(processor, output));
  CHECK_THROWS(QGPlugin::connect(output, output));
  CHECK_THROWS(QGPlugin::connect(uploader, output));

  CHECK_THROWS(QGPlugin::connect(output, plugin));
  CHECK_THROWS(QGPlugin::connect(output, device));
  CHECK_THROWS(QGPlugin::connect(output, processor));
  CHECK_THROWS(QGPlugin::connect(output, output));
  CHECK_NOTHROW(QGPlugin::connect(output, uploader));
}
