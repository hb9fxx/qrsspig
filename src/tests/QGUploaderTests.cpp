#include <QGUploader.h>

TEST_CASE("QGUploaderConnection", "[uploader,connection]") {
  auto plugin = std::shared_ptr<QGPlugin>(new QGPlugin("plugin", 0, YAML::Load("{}")));
  auto device = std::shared_ptr<QGInputDevice>(new QGInputDevice("input", 0, YAML::Load("{}")));
  auto processor = std::shared_ptr<QGProcessor>(new QGProcessor("processor", 0, YAML::Load("{}")));
  auto output = std::shared_ptr<QGOutput>(new QGOutput("output", 0, YAML::Load("{}")));
  auto uploader = std::shared_ptr<QGUploader>(new QGUploader("uploader", 0, YAML::Load("{}")));

  CHECK_THROWS(QGPlugin::connect(plugin, uploader));
  CHECK_THROWS(QGPlugin::connect(device, uploader));
  CHECK_THROWS(QGPlugin::connect(processor, uploader));
  CHECK_NOTHROW(QGPlugin::connect(output, uploader));
  CHECK_THROWS(QGPlugin::connect(uploader, uploader));

  CHECK_THROWS(QGPlugin::connect(uploader, plugin));
  CHECK_THROWS(QGPlugin::connect(uploader, device));
  CHECK_THROWS(QGPlugin::connect(uploader, processor));
  CHECK_THROWS(QGPlugin::connect(uploader, output));
  CHECK_THROWS(QGPlugin::connect(uploader, uploader));
}
