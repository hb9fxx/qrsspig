#include "QGInputDevice.h"
#include "QGOutput.h"
#include "QGProcessor.h"
#include "QGUploader.h"

TEST_CASE("QGInputDevice defaults", "[input]") {
  QGInputDevice device("test", 0, YAML::Load("{}"));
  CHECK(device.type() == "");
  CHECK(device.sampleRate() == 48000);
  CHECK(device.dispFreq() == Approx(0).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device.tunerFreq() == Approx(0).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device.centerFreq() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device.ppm() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));
}

TEST_CASE("QGInputDevice freq params", "[input]") {
  QGInputDevice device1("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000}"));
  CHECK(device1.type() == "");
  CHECK(device1.sampleRate() == 96000);
  CHECK(device1.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device1.tunerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device1.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device1.ppm() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));

  QGInputDevice device2("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, ppm: -5}"));
  CHECK(device2.type() == "");
  CHECK(device2.sampleRate() == 96000);
  CHECK(device2.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device2.tunerFreq() == Approx(7000035.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device2.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device2.ppm() == Approx(-5.).epsilon(std::numeric_limits<double>::epsilon()*100));

  QGInputDevice device3("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, ppm: 2}"));
  CHECK(device3.type() == "");
  CHECK(device3.sampleRate() == 96000);
  CHECK(device3.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device3.tunerFreq() == Approx(6999986.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device3.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device3.ppm() == Approx(2.).epsilon(std::numeric_limits<double>::epsilon()*100));
}

TEST_CASE("QGInputDevice offset freq params", "[input]") {
  QGInputDevice device1("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, offset: 100000000}"));
  CHECK(device1.type() == "");
  CHECK(device1.sampleRate() == 96000);
  CHECK(device1.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device1.tunerFreq() == Approx(107000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device1.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device1.ppm() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));

  QGInputDevice device2("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, offset: 100000000, offsetppm: -2}"));
  CHECK(device2.type() == "");
  CHECK(device2.sampleRate() == 96000);
  CHECK(device2.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device2.tunerFreq() == Approx(106999800.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device2.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device2.ppm() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));

  QGInputDevice device3("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, offset: 100000000, offsetppm: 1}"));
  CHECK(device3.type() == "");
  CHECK(device3.sampleRate() == 96000);
  CHECK(device3.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device3.tunerFreq() == Approx(107000100.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device3.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device3.ppm() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));

  QGInputDevice device4("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, ppm: 2, offset: 100000000, offsetppm: -1}"));
  CHECK(device4.type() == "");
  CHECK(device4.sampleRate() == 96000);
  CHECK(device4.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device4.tunerFreq() == Approx(106999686.0002).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device4.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device4.ppm() == Approx(2.).epsilon(std::numeric_limits<double>::epsilon()*100));

  QGInputDevice device5("test", 0, YAML::Load("{samplerate: 96000, basefreq: 7000000, ppm: -1, offset: 100000000, offsetppm: 2}"));
  CHECK(device5.type() == "");
  CHECK(device5.sampleRate() == 96000);
  CHECK(device5.dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device5.tunerFreq() == Approx(107000307.0002).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device5.centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
  CHECK(device5.ppm() == Approx(-1.).epsilon(std::numeric_limits<double>::epsilon()*100));
}

TEST_CASE("QGInputAlsa", "[input]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("alsa")) {
    std::unique_ptr<QGInputDevice> device;

    // Check type, samplerate and freq/ppm
    REQUIRE_NOTHROW(device = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5}")));
    CHECK(device->type() == "Alsa");
    CHECK(device->sampleRate() == 96000);
    CHECK(device->dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->tunerFreq() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->centerFreq() == Approx(6999965.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->ppm() == Approx(-5.).epsilon(std::numeric_limits<double>::epsilon()*100));

    // Check different channel type are accepted
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: mono}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: left}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: right}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: iq}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: inviq}")));
    CHECK_THROWS(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: alsa, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: stereo}")));
  }
}

TEST_CASE("QGInputHackRF", "[input]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("hackrf")) {
    std::unique_ptr<QGInputDevice> device;

    // Check type, samplerate and freq/ppm
    REQUIRE_NOTHROW(device = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: hackrf, nodevice: true, samplerate: 8000000, basefreq: 7000000, ppm: -5}")));
    CHECK(device->type() == "HackRF");
    CHECK(device->sampleRate() == 8000000);
    CHECK(device->dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->tunerFreq() == Approx(7000035.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->ppm() == Approx(-5.).epsilon(std::numeric_limits<double>::epsilon()*100));

    // Check device serial and index are accepted but not together
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: hackrf, deviceserial: 4242, nodevice: true}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: hackrf, deviceindex: 1, nodevice: true}")));
    CHECK_THROWS(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: hackrf, deviceserial: 4242, deviceindex: 1, nodevice: true}")));
  }
}

TEST_CASE("QGInputLime", "[input]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("limesdr")) {
    std::unique_ptr<QGInputDevice> device;

    // Check type, samplerate and freq/ppm
    REQUIRE_NOTHROW(device = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: limesdr, nodevice: true, samplerate: 8000000, basefreq: 7000000, ppm: -5}")));
    CHECK(device->type() == "LimeSdr");
    CHECK(device->sampleRate() == 8000000);
    CHECK(device->dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->tunerFreq() == Approx(7000035.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->ppm() == Approx(-5.).epsilon(std::numeric_limits<double>::epsilon()*100));
  }
}

TEST_CASE("QGInputPulseAudio", "[input]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("pulseaudio")) {
    std::unique_ptr<QGInputDevice> device;

    // Check type, samplerate and freq/ppm
    REQUIRE_NOTHROW(device = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5}")));
    CHECK(device->type() == "PulseAudio");
    CHECK(device->sampleRate() == 96000);
    CHECK(device->dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->tunerFreq() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->centerFreq() == Approx(6999965.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->ppm() == Approx(-5.).epsilon(std::numeric_limits<double>::epsilon()*100));

    // Check different channel type are accepted
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: mono}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: left}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: right}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: iq}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: inviq}")));
    CHECK_THROWS(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, channel: stereo}")));
  }
}

TEST_CASE("QGInputPulseAudio CreateDevice", "[input]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("pulseaudio")) {
    auto devices = loader->list_devices("pulseaudio");

    // This will ignore [0] auto_null.montior up to [9] ... as long as we don't create more than 8 devices that's ok
    auto num_inputs =  std::count_if(std::begin(devices), std::end(devices), [](auto const & d){ return d.substr(2) != "] auto_null.monitor"; });

    {
      std::unique_ptr<QGInputDevice> device1;
      std::unique_ptr<QGInputDevice> device2;

      REQUIRE_NOTHROW(device1 = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, createdevice: true, device: t1, samplerate: 48000, basefreq: 7000000, ppm: -5}")));

      devices = loader->list_devices("pulseaudio");
      CHECK(std::count_if(std::begin(devices), std::end(devices), [](auto const & d){ return d.substr(2) != "] auto_null.monitor"; }) == num_inputs + 1);

      REQUIRE_NOTHROW(device2 = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: pulseaudio, createdevice: true, device: t2, samplerate: 48000, basefreq: 7000000, ppm: -5}")));

      devices = loader->list_devices("pulseaudio");
      CHECK(std::count_if(std::begin(devices), std::end(devices), [](auto const & d){ return d.substr(2) != "] auto_null.monitor"; }) == num_inputs + 2);
    }

    devices = loader->list_devices("pulseaudio");
    CHECK(std::count_if(std::begin(devices), std::end(devices), [](auto const & d){ return d.substr(2) != "] auto_null.monitor"; }) == num_inputs);
  }
}

TEST_CASE("QGInputRtlSdr", "[input]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("rtlsdr")) {
    std::unique_ptr<QGInputDevice> device;

    // Check type, samplerate and freq/ppm
    REQUIRE_NOTHROW(device = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 240000, basefreq: 7000000, ppm: -5.2}")));
    CHECK(device->type() == "RtlSdr");
    CHECK(device->sampleRate() == 240000);
    CHECK(device->dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->tunerFreq() == Approx(7000001.4).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->centerFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->ppm() == Approx(-.2).epsilon(std::numeric_limits<double>::epsilon()*100));

    // Check different settings for directsampling are accepted
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, directsampling: false}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, directsampling: true}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, directsampling: true, directsamplingbranch: i-branch}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, directsampling: true, directsamplingbranch: q-branch}")));
    CHECK_THROWS(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, directsampling: maybe}")));
    CHECK_THROWS(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: rtlsdr, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, directsampling: true, directsamplingbranch: y-branch}")));
  }
}

TEST_CASE("QGInputStdin", "[input,stdin]") {
  auto loader = std::make_unique<QGPluginLoader<QGInputDevice>>("input");

  if (loader->has_dl("stdin")) {
    std::unique_ptr<QGInputDevice> device;

    // Check type, samplerate and freq/ppm
    REQUIRE_NOTHROW(device = QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5}")));
    CHECK(device->type() == "Stdin");
    CHECK(device->sampleRate() == 96000);
    CHECK(device->dispFreq() == Approx(7000000.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->tunerFreq() == Approx(0.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->centerFreq() == Approx(6999965.).epsilon(std::numeric_limits<double>::epsilon()*100));
    CHECK(device->ppm() == Approx(-5.).epsilon(std::numeric_limits<double>::epsilon()*100));

    // Check different format type are accepted
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: u8iq}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: s8iq}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: u16iq}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: s16iq}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: s16mono}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: s16left}")));
    CHECK_NOTHROW(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: s16right}")));
    CHECK_THROWS(QGInputDevice::CreateInputDevice(0, YAML::Load("{type: stdin, nodevice: true, samplerate: 96000, basefreq: 7000000, ppm: -5, format: straight}")));
  }
}

TEST_CASE("QGInputDeviceConnection", "[input,connection]") {
  auto plugin = std::shared_ptr<QGPlugin>(new QGPlugin("plugin", 0, YAML::Load("{}")));
  auto device = std::shared_ptr<QGInputDevice>(new QGInputDevice("input", 0, YAML::Load("{}")));
  auto processor = std::shared_ptr<QGProcessor>(new QGProcessor("processor", 0, YAML::Load("{}")));
  auto output = std::shared_ptr<QGOutput>(new QGOutput("output", 0, YAML::Load("{}")));
  auto uploader = std::shared_ptr<QGUploader>(new QGUploader("uploader", 0, YAML::Load("{}")));

  CHECK_THROWS(QGPlugin::connect(plugin, device));
  CHECK_THROWS(QGPlugin::connect(device, device));
  CHECK_THROWS(QGPlugin::connect(processor, device));
  CHECK_THROWS(QGPlugin::connect(output, device));
  CHECK_THROWS(QGPlugin::connect(uploader, device));

  CHECK_THROWS(QGPlugin::connect(device, plugin));
  CHECK_THROWS(QGPlugin::connect(device, device));
  CHECK_NOTHROW(QGPlugin::connect(device, processor));
  CHECK_THROWS(QGPlugin::connect(device, output));
  CHECK_THROWS(QGPlugin::connect(device, uploader));
}
