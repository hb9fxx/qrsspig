#pragma once

#include "Config.h"

#include <cmath>
#include <complex> // Must be included before fftw3
#include <exception>
#include <memory>
#include <vector>

#include <fftw3.h>

class QGFFT {
// TODO: enable to chose patient or exaustive plan creation. Save and restore plan
// TODO: use tempate for std::complex<float>
public:
	explicit QGFFT(int n, bool backward = false) :
	_n(n),
	_in(static_cast<std::complex<float>*>(fftwf_malloc(sizeof(std::complex<float>) * _n))),
	_out(static_cast<std::complex<float>*>(fftwf_malloc(sizeof(std::complex<float>) * _n))),
	_plan(
		static_cast<fftwf_plan_s*>(fftwf_plan_dft_1d(
			_n,
			reinterpret_cast<fftwf_complex*>(_in.get()),
			reinterpret_cast<fftwf_complex*>(_out.get()),
			backward ? FFTW_BACKWARD : FFTW_FORWARD,
			FFTW_MEASURE
		))
	) {};

	void execute() {
		fftwf_execute(_plan.get());
	}

	int n() { return _n; };
	std::complex<float>* in() { return _in.get(); };
	std::complex<float>* out() { return _out.get(); };

	int freq2Bucket(double freq, int sampleRate) {
		if ((freq <= -sampleRate/2) || (freq > sampleRate/2)) {
			throw std::runtime_error("Frequency out of range");
		}

		int b = freq * _n / sampleRate;

		if (freq < 0) {
			return b + _n;
		}

		return b;
	}

	double bucket2Freq(int bucket, int sampleRate) {
		if ((bucket < 0) || (bucket >= _n)) {
			throw std::runtime_error("Bucket out of range");
		}

		double f = static_cast<double>(bucket) * sampleRate / _n;

		if (bucket > _n/2) {
			f -= sampleRate;
		}

		return f;
	}

	// N for at least a resolution of res Hz/px
	static int n4res(float res, int sampleRate) {
		return pow(2, ceil(log2(sampleRate / res)));
	}

private:
	const int _n;

	struct _fftwDeleter {
		void operator()(std::complex<float>* b) {
			fftwf_free(b);
		}
	};

	struct _fftwPlanDeleter {
		void operator()(fftwf_plan_s* p) {
			fftwf_destroy_plan(p);
		}
	};

	std::unique_ptr<std::complex<float>[], _fftwDeleter> _in;
	std::unique_ptr<std::complex<float>[], _fftwDeleter> _out;
	std::unique_ptr<fftwf_plan_s, _fftwPlanDeleter> _plan;
};
