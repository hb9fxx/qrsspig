#pragma once

#include <chrono>
#include <complex>
#include <functional>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include "Config.h"
#include <dlfcn.h>

#include <boost/filesystem.hpp>

class NoListDevicesException: public std::exception
{
  virtual const char* what() const throw()
  {
    return "No list_devices method found in plugin";
  }
};

template <class T>
class QGPluginLoader {
protected:
	std::string prefix;

public:
  static std::string plugin_path() {
    return std::string(INSTALL_PREFIX) + "/lib/qrsspig/";
  }

	explicit QGPluginLoader(const std::string& prefix) : prefix{prefix} {};

	virtual ~QGPluginLoader() {};

	T* create(std::string const & type, int index, YAML::Node const &config);

  bool has_dl(std::string type);
	std::vector<std::string> list_plugins();
	bool has_module_info(std::string type);
	std::string module_info(std::string type);
	bool has_list_devices(std::string type);
	std::vector<std::string> list_devices(std::string type);
private:
  void *load_dl(std::string type);

	std::string build_path(std::string &type) {
		return plugin_path() + "lib" + prefix + "_" + type + ".so";
	}
};


template <class T>
void* QGPluginLoader<T>::load_dl(std::string type) {
	std::string path = type;

  if (type.compare(0, 1, "/") != 0) {
		path = this->build_path(type);
	}

	void* handle = dlopen(path.c_str(), RTLD_LAZY);
	if (!handle) {
		throw std::runtime_error(std::string("QGPluginLoader: can't open ") + path);
	}

	return handle;
}

template <class T>
T* QGPluginLoader<T>::create(std::string const & type, int index, YAML::Node const &config) {
	void*handle = this->load_dl(type);
	T* (*create)(const YAML::Node &config, int index);
	create = (T* (*)(const YAML::Node &config, int index))dlsym(handle, "create_object");

	if (!create) {
		throw std::runtime_error(std::string("QGPluginLoader: can't load create from ") + type);
	}

	T* obj = (T*)create(config, index);

	if (!obj) {
		throw std::runtime_error(std::string("QGPluginLoader: can't create ") + type);
	}

	return obj;
}

template <class T>
bool QGPluginLoader<T>::has_dl(std::string type) {
  std::string path = type;

  if (type.compare(0, 1, "/") != 0) {
    path = this->build_path(type);
  }

  void* handle = dlopen(path.c_str(), RTLD_LAZY);

  if (!handle) {
    return false;
  }

  return true;
}

template <class T>
std::vector<std::string> QGPluginLoader<T>::list_plugins() {
	struct path_leaf_string
    {
	    std::string operator()(const boost::filesystem::directory_entry& entry) const {
		    return entry.path().leaf().string();
	    }
    };

	std::string begins_with = "lib" + prefix + "_";

    boost::filesystem::path p(plugin_path());
    boost::filesystem::directory_iterator start(p);
    boost::filesystem::directory_iterator end;
    std::vector<std::string> v;
    std::transform(start, end, std::back_inserter(v), path_leaf_string());
    std::vector<std::string> v2;
    std::copy_if(v.begin(),v.end(),std::back_inserter(v2), [&begins_with](const std::string i) {
		    return i.find(begins_with) == 0 && i.find(".so") == i.length()-3;
	    });
    std::vector<std::string> v3;
    std::transform(v2.begin(),v2.end(),std::back_inserter(v3), [&begins_with](const std::string i) {
		    std::string x = i;
		    x.erase(0,begins_with.length());
		    x.erase(x.length()-3,x.length());
		    return x;
	    });
    std::sort(std::begin(v3), std::end(v3));

    return v3;
}

template <class T>
bool QGPluginLoader<T>::has_module_info(std::string type) {
	void*handle = this->load_dl(type);
	std::vector<std::string> (*module_info)();
	module_info = (std::vector<std::string> (*)())dlsym(handle, "module_info");
	return module_info != NULL;
}

template <class T>
std::string QGPluginLoader<T>::module_info(std::string type) {
	void*handle = this->load_dl(type);
	std::string (*module_info)();
	module_info = (std::string (*)())dlsym(handle, "module_info");
	if (!module_info) {
		throw std::runtime_error(std::string("QGPluginLoader: Failed to load 'module_info' for ") + type);
	}
	return module_info();
}

template <class T>
bool QGPluginLoader<T>::has_list_devices(std::string type) {
	void*handle = this->load_dl(type);
	std::vector<std::string> (*list_devices)();
	list_devices = (std::vector<std::string> (*)())dlsym(handle, "list_devices");
	return list_devices != NULL;
}

template <class T>
std::vector<std::string> QGPluginLoader<T>::list_devices(std::string type) {
	void*handle = this->load_dl(type);
	std::vector<std::string> (*list_devices)();
	list_devices = (std::vector<std::string> (*)())dlsym(handle, "list_devices");
	if (!list_devices) {
		throw std::runtime_error(std::string("QGPluginLoader: Failed to load 'list_devices' for ") + type);
	}
	return list_devices();
}
