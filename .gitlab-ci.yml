image: debian:bullseye

variables: &global_variables
  VERSION: 0.8.0
  DEBREVISION: 1
  IMAGEVERSION: 0.8.0

stages:
  - build
  - test
  - report
  - package-src
  - package-bin
  - package-test
  - package-bundle
  - package-upload

build:linux:
  stage: build
  script:
    - apt-get update -qq && apt-get install -y -qq build-essential clang cmake
    - apt-get install -y -qq libboost-filesystem-dev libfftw3-dev libliquid-dev libyaml-cpp-dev
    - apt-get install -y -qq libairspy-dev libasound-dev gr-osmosdr libhackrf-dev liblimesuite-dev libpulse-dev librtlsdr-dev
    - apt-get install -y -qq libgd-dev
    - apt-get install -y -qq libcurl4-openssl-dev libssh-dev
    - mkdir -p build
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX=. -DWITH_INPUT_AIRSPY=ON -DWITH_INPUT_GROSMOSDR=ON -DWITH_INPUT_LIMESDR=ON ..
    - make -j 4
    - make -j 4 install
  artifacts:
    expire_in: 1 day
    paths:
      - build/bin/
      - build/lib/

build:tar:
  stage: build
  only:
    - staging
    - master
  script:
    - apt-get update -qq && apt-get install -y -qq xz-utils
    - mkdir -p build/deb/qrsspig-${VERSION}
    - cp -a cmake debian etc src systemd CMakeLists.txt ChangeLog LICENSE README.md THANKS qrsspig.yaml.template build/deb/qrsspig-${VERSION}
    - cd build/deb
    - tar cJf qrsspig_${VERSION}.orig.tar.xz qrsspig-${VERSION}
  artifacts:
    expire_in: 1 day
    paths:
      - build/deb/qrsspig-${VERSION}
      - build/deb/qrsspig_${VERSION}.orig.tar.xz

unittests:
  stage: test
  script:
    - apt-get update -qq && apt-get install -y -qq libboost-filesystem1.74.0 libyaml-cpp0.6 libfftw3-bin libgd3
    - apt-get install -y -qq librtlsdr0 libhackrf0 libasound2 liblimesuite20.10-1 gr-osmosdr libairspy0 pulseaudio
    - apt-get install -y -qq libliquid2d
    - apt-get install -y -qq libcurl4 libssh-4
    - sed -i "s/load-module module-native-protocol-unix/load-module module-native-protocol-unix auth-anonymous=1/g" /etc/pulse/system.pa
    - pulseaudio --system --daemonize
    - ./build/bin/qrsspig --runtests
    - ./build/bin/qrsspig --listmodules --listdevices
  needs:
  - job: build:linux
    artifacts: true

unittests:valgrind:
  stage: test
  allow_failure: true
  script:
    - apt-get update -qq && apt-get install -y -qq libboost-filesystem1.74.0 libyaml-cpp0.6 libfftw3-bin libgd3
    - apt-get install -y -qq librtlsdr0 libhackrf0 libasound2 liblimesuite20.10-1 gr-osmosdr libairspy0 pulseaudio
    - apt-get install -y -qq libliquid2d
    - apt-get install -y -qq libcurl4 libssh-4
    - apt-get install -y -qq valgrind
    - sed -i "s/load-module module-native-protocol-unix/load-module module-native-protocol-unix auth-anonymous=1/g" /etc/pulse/system.pa
    - pulseaudio --system --daemonize
    - valgrind --leak-check=full --error-exitcode=42 --xml=yes --xml-file=valgrind1.xml ./build/bin/qrsspig --runtests
    - valgrind --leak-check=full --error-exitcode=42 --suppressions=valgrind.supp --gen-suppressions=all --xml=yes --xml-file=valgrind2.xml ./build/bin/qrsspig --listmodules --listdevices
  artifacts:
    when: on_failure
    expire_in: 1 day
    paths:
      - valgrind1.xml
      - valgrind2.xml
  needs:
  - job: build:linux
    artifacts: true

unittests:cppcheck:
  stage: test
  allow_failure: true
  image: registry.gitlab.com/hb9fxx/qrsspig/cppcheck:1.87
  script:
    - cppcheck --quiet --force --enable=all --suppressions-list=cppcheck.supp --xml --xml-version=2 --error-exitcode=42 src 2> cppcheck.xml
  artifacts:
    when: always
    expire_in: 1 day
    paths:
      - cppcheck.xml
  needs: []

report:cppcheck:
  stage: report
  allow_failure: true
  image: registry.gitlab.com/hb9fxx/qrsspig/cppcheck:1.87
  script:
    - mkdir -p reports/cppcheck/
    - cppcheck-htmlreport --file=cppcheck.xml --report-dir=reports/cppcheck/ --source-dir=.
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - reports/cppcheck/
  needs:
  - job: unittests:cppcheck
    artifacts: true

.package_build_source: &package_build_source
  stage: package-src
  only:
  - staging
  - master
  needs:
  - job: build:tar
    artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_${DIST}:${IMAGEVERSION}
  script:
  - cd build/deb
  - mkdir ${DIST}
  - mv qrsspig* ${DIST}/
  - cd ${DIST}/qrsspig-${VERSION}
  - cp debian/control.${DIST} debian/control
  - sed -i "s/RELEASED/${DIST}/g" debian/changelog
  - gpg --no-tty --import $GPG_PRIVATE_KEY
  - debuild -eDEB_BUILD_OPTIONS="parallel=${PARALLEL}" -i -S
  artifacts:
    expire_in: 1 week
    paths:
    - build/deb/${DIST}/qrsspig-${VERSION}
    - build/deb/${DIST}/qrsspig_${VERSION}.orig.tar.xz
    - build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}.debian.tar.xz
    - build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}.dsc
    - build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}_source.build*
    - build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}_source.changes

.package_build_binary:
  script: &package_build_binary
    - cd build/deb/${DIST}/qrsspig-${VERSION}
    - gpg --no-tty --import $GPG_PRIVATE_KEY
    - debuild -eDEB_BUILD_OPTIONS="parallel=${PARALLEL}" -i -b

.package_binary_paths:
  paths: &package_binary_paths
    - build/deb/${DIST}/qrsspig*_${VERSION}-${DEBREVISION}_${ARCH}.*deb
    - build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}_${ARCH}.build*
    - build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}_${ARCH}.changes

.package_test: &package_test
  stage: package-test
  only:
  - staging
  - master

  script:
  - apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -y -qq gdebi pulseaudio
  - sed -i "s/load-module module-native-protocol-unix/load-module module-native-protocol-unix auth-anonymous=1/g" /etc/pulse/system.pa
  - pulseaudio --system --daemonize
  - DEBIAN_FRONTEND=noninteractive gdebi -nq build/deb/${DIST}/qrsspig_${VERSION}-${DEBREVISION}_${ARCH}.deb
  - qrsspig --runtests
  - qrsspig --listmodules --listdevices

# var substitution not supported in image key yet
.package_test_debian: &package_test_debian
  <<: *package_test
  image: debian:{DIST}}

.package_test_ubuntu: &package_test_ubuntu
  <<: *package_test
  image: ubuntu:{DIST}}

# Debian Stretch
package:source:stretch:
  <<: *package_build_source
  variables:
    <<: *global_variables
    DIST: stretch
    PARALLEL: 4

package:binary:stretch:i386:
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: stretch
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:stretch
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_i386_stretch:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:stretch:amd64:
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: stretch
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:stretch
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_stretch:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:stretch:armhf:
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: stretch
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:stretch
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm32v7_stretch:${IMAGEVERSION}
  tags:
    - armhf
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:stretch:arm64:
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: stretch
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:stretch
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm64v8_stretch:${IMAGEVERSION}
  tags:
    - arm64
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:test:stretch:i386:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: stretch
  needs:
  - job: package:binary:stretch:i386
    artifacts: true
  image: i386/debian:stretch

package:test:stretch:amd64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: stretch
  needs:
  - job: package:binary:stretch:amd64
    artifacts: true
  image: amd64/debian:stretch

package:test:stretch:armhf:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: stretch
  needs:
  - job: package:binary:stretch:armhf
    artifacts: true
  image: arm32v7/debian:stretch
  tags:
  - armhf

package:test:stretch:arm64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: stretch
  needs:
  - job: package:binary:stretch:arm64
    artifacts: true
  image: arm64v8/debian:stretch
  tags:
  - arm64

# Debian Buster
package:source:buster:
  <<: *package_build_source
  variables:
    <<: *global_variables
    DIST: buster
    PARALLEL: 4

package:binary:buster:i386:
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: buster
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:buster
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_i386_buster:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:buster:amd64:
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: buster
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:buster
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_buster:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:buster:armhf:
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: buster
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:buster
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm32v7_buster:${IMAGEVERSION}
  tags:
    - armhf
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:buster:arm64:
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: buster
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:buster
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm64v8_buster:${IMAGEVERSION}
  tags:
    - arm64
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:test:buster:i386:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: buster
  needs:
  - job: package:binary:buster:i386
    artifacts: true
  image: i386/debian:buster

package:test:buster:amd64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: buster
  needs:
  - job: package:binary:buster:amd64
    artifacts: true
  image: amd64/debian:buster

package:test:buster:armhf:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: buster
  needs:
  - job: package:binary:buster:armhf
    artifacts: true
  image: arm32v7/debian:buster
  tags:
  - armhf

package:test:buster:arm64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: buster
  needs:
  - job: package:binary:buster:arm64
    artifacts: true
  image: arm64v8/debian:buster
  tags:
  - arm64

# Debian Bullseye
package:source:bullseye:
  <<: *package_build_source
  variables:
    <<: *global_variables
    DIST: bullseye
    PARALLEL: 4

package:binary:bullseye:i386:
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: bullseye
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bullseye
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_i386_bullseye:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:bullseye:amd64:
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: bullseye
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bullseye
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_bullseye:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:bullseye:armhf:
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: bullseye
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bullseye
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm32v7_bullseye:${IMAGEVERSION}
  tags:
    - armhfonly
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:bullseye:arm64:
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: bullseye
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bullseye
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm64v8_bullseye:${IMAGEVERSION}
  tags:
    - arm64
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:test:bullseye:i386:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: bullseye
  needs:
  - job: package:binary:bullseye:i386
    artifacts: true
  image: i386/debian:bullseye

package:test:bullseye:amd64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: bullseye
  needs:
  - job: package:binary:bullseye:amd64
    artifacts: true
  image: amd64/debian:bullseye

package:test:bullseye:armhf:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: bullseye
  needs:
  - job: package:binary:bullseye:armhf
    artifacts: true
  image: arm32v7/debian:bullseye
  tags:
  - armhfonly

package:test:bullseye:arm64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: bullseye
  needs:
  - job: package:binary:bullseye:arm64
    artifacts: true
  image: arm64v8/debian:bullseye
  tags:
  - arm64

# Ubuntu Bionic
package:source:bionic:
  <<: *package_build_source
  variables:
    <<: *global_variables
    DIST: bionic
    PARALLEL: 4

package:binary:bionic:i386:
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: bionic
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bionic
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_i386_bionic:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:bionic:amd64:
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: bionic
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bionic
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_bionic:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:bionic:armhf:
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: bionic
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bionic
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm32v7_bionic:${IMAGEVERSION}
  tags:
    - armhf
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:bionic:arm64:
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: bionic
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:bionic
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm64v8_bionic:${IMAGEVERSION}
  tags:
    - arm64
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:test:bionic:i386:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: i386
    DIST: bionic
  needs:
  - job: package:binary:bionic:i386
    artifacts: true
  image: i386/ubuntu:bionic

package:test:bionic:amd64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: bionic
  needs:
  - job: package:binary:bionic:amd64
    artifacts: true
  image: amd64/ubuntu:bionic

package:test:bionic:armhf:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: armhf
    DIST: bionic
  needs:
  - job: package:binary:bionic:armhf
    artifacts: true
  image: arm32v7/ubuntu:bionic
  tags:
  - armhf

package:test:bionic:arm64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: bionic
  needs:
  - job: package:binary:bionic:arm64
    artifacts: true
  image: arm64v8/ubuntu:bionic
  tags:
  - arm64

# Ubuntu Focal
package:source:focal:
  <<: *package_build_source
  variables:
    <<: *global_variables
    DIST: focal
    PARALLEL: 4

package:binary:focal:amd64:
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: focal
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:focal
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_focal:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:focal:arm64:
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: focal
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:focal
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm64v8_focal:${IMAGEVERSION}
  tags:
    - arm64
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:test:focal:amd64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: focal
  needs:
  - job: package:binary:focal:amd64
    artifacts: true
  image: amd64/ubuntu:focal

package:test:focal:arm64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: focal
  needs:
  - job: package:binary:focal:arm64
    artifacts: true
  image: arm64v8/ubuntu:focal
  tags:
  - arm64

# Ubuntu Jammy
package:source:jammy:
  <<: *package_build_source
  variables:
    <<: *global_variables
    DIST: jammy
    PARALLEL: 4

package:binary:jammy:amd64:
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: jammy
    PARALLEL: 4
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:jammy
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_amd64_jammy:${IMAGEVERSION}
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:binary:jammy:arm64:
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: jammy
    PARALLEL: 1
  stage: package-bin
  only:
    - staging
    - master
  needs:
    - job: package:source:jammy
      artifacts: true
  image: registry.gitlab.com/hb9fxx/qrsspig/build_arm64v8_jammy:${IMAGEVERSION}
  tags:
    - arm64
  script: *package_build_binary
  artifacts:
    expire_in: 1 week
    paths: *package_binary_paths

package:test:jammy:amd64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: amd64
    DIST: jammy
  needs:
  - job: package:binary:jammy:amd64
    artifacts: true
  image: amd64/ubuntu:jammy

package:test:jammy:arm64:
  <<: *package_test
  variables:
    <<: *global_variables
    ARCH: arm64
    DIST: jammy
  needs:
  - job: package:binary:jammy:arm64
    artifacts: true
  image: arm64v8/ubuntu:jammy
  tags:
  - arm64

package:bundle:
  stage: package-bundle
  only:
    - staging
    - master
  script:
    - apt-get update -qq && apt-get install -y -qq xz-utils
    - cd build/deb
    - tar cJf qrsspig_${VERSION}-${DEBREVISION}.tar.xz stretch buster bullseye bionic focal jammy
    - mv qrsspig_${VERSION}-${DEBREVISION}.tar.xz ../../
  artifacts:
    paths:
      - qrsspig_${VERSION}-${DEBREVISION}.tar.xz
      - cppcheck.xml
      - valgrind1.xml
      - valgrind2.xml

package:upload:
  stage: package-upload
  only:
    - master
  script:
    - apt-get update -qq && apt-get install -y -qq openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - scp -o 'StrictHostKeyChecking no' -P 11235 qrsspig_${VERSION}-${DEBREVISION}.tar.xz gitlab@debian.hb9fxx.ch:incoming/
