#!/bin/sh

tag=$1

# arch must be either arm32v7, arm64v8, i386 or amd64
arch=$2

shift 2

for dist in "$@"
do
	docker build -t registry.gitlab.com/hb9fxx/qrsspig/build_$arch\_$dist:latest -t registry.gitlab.com/hb9fxx/qrsspig/build_$arch\_$dist:$tag --build-arg ARCH=$arch -f docker/Dockerfile_build_$dist docker/
	docker push registry.gitlab.com/hb9fxx/qrsspig/build_$arch\_$dist:latest
	docker push registry.gitlab.com/hb9fxx/qrsspig/build_$arch\_$dist:$tag
done
